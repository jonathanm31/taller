<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Motorcenter</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>

    <script src="{{ asset('assets/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('assets/js/loadingoverlay_progress.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/lista.css') }}" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


    <!-- /core JS files -->


    <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/color/spectrum.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/core/app.js') }}"></script>
    <!-- /theme JS files -->
    <style type="text/css">

    </style>

</head>

<body class="sidebar-xs has-detached-left">

    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Motorcenter</a>

            <ul class="nav navbar-nav pull-right visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                <li><a class="sidebar-mobile-detached-toggle"><i class="icon-grid7"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="assets/images/image.png" alt="">
                        <span>Victoria</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                        <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
                        <li><a href="#"><span class="badge badge-warning pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                        <li><a href="/logout"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <div class="sidebar-user">
                        <div class="category-content">
                            <div class="media">
                                <a href="#" class="media-left"><img src="assets/images/image.png" class="img-circle img-sm" alt=""></a>
                                <div class="media-body">
                                    <span class="media-heading text-semibold">Victoria Baker</span>
                                    <div class="text-size-mini text-muted">
                                        <i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
                                    </div>
                                </div>

                                <div class="media-right media-middle">
                                    <ul class="icons-list">
                                        <li>
                                            <a href="#"><i class="icon-cog3"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /user menu -->


                    <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">

                                <!-- Main -->
                                <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                                <li><a href=""><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                                <li><a href="list_clientes"><i class=" icon-users2"></i> <span>Clientes</span></a></li>
                                <li><a href="caja"><i class="glyphicon glyphicon-usd"></i> <span>Caja</span></a></li>
                                <li>
                                    <a href="#"><i class="icon-grid"></i> <span>Productos</span></a>
                                    <ul>
                                        <li><a href="list_product"><i class="icon-cube"></i>Productos</a></li>
                                        <li><a href="list_servicios"><i class="glyphicon glyphicon-wrench"></i>Servicios</a> </li>

                                        <li><a href="list_categorys"><i class="icon-list"></i>Categorias</a></li>
                                        <li><a href="list_marcas"><i class=" icon-racing"></i>Marcas</a></li>
                                        <li><a href="list_motos"><i class="icon-road"></i>Motos</a> </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#"><i class="glyphicon glyphicon-retweet"></i> <span>Inventario</span></a>
                                    <ul>
                                        <li><a href="entradas"><i class="glyphicon glyphicon-resize-small"></i>Entradas</a></li>
                                        <li><a href="salidas"><i class="glyphicon glyphicon-resize-full"></i>Salidas</a> </li>

                                    </ul>
                                </li>

                                <li><a href="crear_documento"><i class="icon-stack"></i> <span>Documentos</span></a></li>
                                <!-- /main -->

                            </ul>
                        </div>
                    </div>
                    <!-- /main navigation -->

                </div>
            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            @if (!Auth::guest())
                                @yield('titulo')
                            @endif

                        </div>

                        <div class="heading-elements">
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            @if (!Auth::guest())
                                @yield('breadcrumb')
                            @endif

                        </ul>

                        <ul class="breadcrumb-elements">

                            @if (!Auth::guest())
                                @yield('menu')
                            @endif

                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Ayuda</a></li>

                        </ul>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                    <!-- Detached content -->
                    <div class="container-detached">

                                @if (!Auth::guest())
                                    @yield('contenido')
                                @endif

                    </div>
                    <!-- /detached content -->


                    <!-- Footer -->
                    <div class="footer text-muted">
                        &copy; 2017. MotorCenter <a href="#">desarrollado por BitsKnow</a>
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->
    <script type="application/javascript">
        $(document).ajaxSend(function(){
            $.LoadingOverlay("show", {
                fade  : [2000, 1000]
            });
        });
        $(document).ajaxComplete(function(){
            $.LoadingOverlay("hide");
        });
    </script>
</body>
</html>
