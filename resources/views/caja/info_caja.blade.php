<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Motorcenter</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js') }}"></script>


    <!-- /theme JS files -->
    <style type="text/css">

    </style>

</head>
<body style="margin:20px; padding: 10px;">
<div class="cabecera" style="">
    <div class="boleta">
        <h2>
            <span id="tipo_bol">
                <?PHP
                    if($caja->tipo > 1){
                        echo "FACTURA";

                    }else{
                        echo "BOLETA";

                    }
                ?>
            </span> : <span id="serie"><?PHP echo str_pad($sucursal->serie, 3, "0", STR_PAD_LEFT);  ?></span> - <span id="numeracio"><?PHP echo str_pad($caja->numeracion, 6, "0", STR_PAD_LEFT);  ?></span>
        </h2>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <h3 id="bol_empresa"></h3>
        </div>
        <div class="col-sm-6 text-right" id="datos-recibo">
            <b>FECHA:</b>
            <span id="bol_fecha">{{ $caja->created_at }}</span><br>
            <b>DIRECCION:</b>
            <span id="bol_sucursal">{{ $sucursal->direccion  }}</span><br>
            <b>RUC</b>
            <span id="bol_empresaRuc">{{ $sucursal->ruc  }}</span><br>
            <b>TELEFONO</b>
            <span id="bol_empresaTelefono">{{ $sucursal->telefono  }}</span><br>
        </div>

    </div>
</div>
<div class="contenido" >
    <div class="row">
        <div class="col-sm-6" id="datos_cliente">
            <h5>CLIENTE:</h5>
            <b>NOMBRES:</b>
            <span id="bol_cliente">{{ $cliente->nombres }} {{$cliente->apellidos}}</span><br>
            <b>DNI:</b>
            <span id="bol_clienteDni">{{ $cliente->dni }}</span><br>
            <b>RUC:</b>
            <span id="bol_clienteRUC"> {{ $cliente->ruc  }}</span><br>
            <b>DIRECCION:</b>
            <span id="bol_clienteDireccion">{{ $cliente->direccion  }}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table  class="table" id="validar_boleta">
                <thead>
                <tr>
                    <th>PRODUCTO</th>
                    <th>PRECIO /U.</th>
                    <th>CANTIDAD</th>
                    <th>IMPORTE</th>
                </tr>
                </thead>
                <tbody id="bol_detalle">
                @foreach ($cajaD as $product)
                    <tr>

                        <td><b>{{$product->nombre}}</b></td>
                        <td>S/. {{ round($product->precio,2)}}</td>
                        <td>u/ {{$product->cantidad}}</td>
                        <td>{{$product->precio}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 20px">
        <div class="col-sm-2 col-sm-offset-9 col-md-2 col-md-offset-9">
            <table  class="table" id="validar_boleta">
                <tr>
                    <th>TOTAL</th>
                    <td id="bol_total">{{ $caja->total }}</td>
                </tr>
                <tr>
                    <th>PAGO</th>
                    <td id="bol_pago">{{ $caja->paga }}</td>
                </tr>
                <tr>
                    <th>VUELTO</th>
                    <td id="bol_vuelto">{{ $caja->vuelto  }}</td>
                </tr>
                <tr>
                    <th>IGV</th>
                    <td id="bol_igv"> {{ $caja->igv }}</td>
                </tr>
                <tr>
                    <th>DESCUENTO</th>
                    <td id="bol_descuento"> {{ $caja->descuento }}</td>
                </tr>
            </table>
        </div>

    </div>
    <div class="row" >
        <div class="col-sm-12" style="border:solid 1px #2D2D2D; padding: 10px; font-size: 14px; height: auto">

            <h2>DETALLES</h2>
            <div  id="comentarios" >
            </div>
        </div>

    </div>

    <script type="application/javascript" rel="script">
        var html_comments = '{{ $caja->comentarios }}';
        html_comments = $("<div />").html(html_comments).text();

        $('#comentarios').append(html_comments);
    </script>

</div>
</body>