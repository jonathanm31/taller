@extends('index')

<!-- TITULO PAGINA -->

@section('titulo')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Caja / Efectua una Venta</span></h4>
@stop

<!--BREADCRUMB -->
@section('breadcrumb')
    <li><a href="/"><i class="icon-home2 position-left"></i> Home</a></li>
    <li>Caja</li>
@stop
<!-- MENU AUXLIAR -->

@section('menu')

@stop

<!-- CONTENIDO DE LA PAGINA -->

@section('contenido')
    <?PHP
    header("Access-Control-Allow-Origin:*");
    ?>

<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ URL::to('/') }}">

    <div class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-8">
                            <fieldset>
                                <legend>Cliente:</legend>
                            <div class="col-xs-12 col-md-4">
                                <div class="from-group" id="dni_group">
                                    <label for="dni">*DNI:</label>
                                    <input type="text" class="form-control input-lg"  id="cliente_search"  placeholder="Buscar nombre, apellido, DNI o RUC">
                                    <input type="hidden" class="form-control" id="idcliente" value="">
                                    <div id="search_results" class="list-group col-md-12 hide"></div>
                                </div>
                                <div class="from-group" id="ruc_group" hidden>
                                    <label for="ruc">RUC:</label>
                                    <input type="text" class="form-control" id="ruc" value="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <div class="from-group" id="nombre_group">
                                    <label for="nombres">*Nombres:</label>
                                    <input type="text" class="form-control" id="nombres" placeholder="Nombres" value="">
                                </div>
                                <div class="from-group" id="direccion_group">
                                    <label for="direccion">Direccion:</label>
                                    <input type="text" class="form-control" id="direccion" placeholder="Direccion." value="">
                                </div>
                            </div>

                            </fieldset>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                        <div class="form-group ">
                                            <button class="btn btn-info form-control btn-lg" id="btn_guardar" data-idprod="9">
                                                <i class="glyphicon glyphicon-save"></i>
                                                Guardar
                                            </button>
                                        </div>
                                        <div class="form-group ">
                                            <select class="form-control" id="documento">
                                                <option value="" disabled="disabled" >TIPO RECIBO</option>
                                                <option value="1" selected>Boleta</option>
                                                <option value="2">Factura</option>
                                            </select>
                                        </div>
                                        <div class="form-group ">
                                            <button type="button" class="btn btn-default form-control btn-lg" id="eliminar_all" >
                                                <i class="glyphicon glyphicon-trash"></i>
                                                Limpiar
                                            </button>
                                        </div>
                            </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-7">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <table  class="table table-borderless">
                            <thead>
                                <tr class="bg-success-700">
                                    <th>Cantidad</th>
                                    <th>Producto</th>
                                    <th>Precio Unitario</th>
                                    <th>Descuento %</th>
                                    <th>Precio Total</th>
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tbody id="lista_carrito" >
                            </tbody>
                            <tfoot style="text-align: right">
                                <tr>
                                    <td class="bg-success-400" colspan="3">
                                       (0.18%)  IGV S/.<br>

                                    </td>
                                    <td colspan="2">
                                        <div class="alert alert-warning" role="alert">
                                            <input class="form-control pull-left" id="igv" type="number" value="0.00" step="0.1" disabled>
                                            <br>
                                            <label class=""><input  class="pull-left" type="checkbox" id="con_igv" checked>Precios incluyen IGV</label>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="bg-success-400" colspan="3">
                                        Descuento S/.
                                    </td>
                                    <td colspan="2">
                                      <input class="form-control" id="descuento" type="number" value="0.00" step="0.1" >
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bg-success-400" colspan="3">
                                        Paga con S/.
                                    </td>
                                    <td colspan="2">
                                        <input class="form-control" id="paga"  type="number" value="0.00" step="0.1" >
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bg-success-400" colspan="3">
                                        Total S/.
                                    </td>
                                    <td colspan="2">
                                        <input class="form-control" id="total"  type="number" value="0.00" step="0.1" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bg-success-800" colspan="3">
                                        Vuelto S/.
                                    </td>
                                    <td colspan="2">
                                        <input class="form-control" id="vuelto"  type="number" value="0.00" step="0.1" disabled>
                                    </td>
                                </tr>

                            </tfoot>

                        </table>

                        <div class="form-group">
                            <h5><label for="comment">DETALLE:</label></h5>
                            <textarea class="form-control" rows="20" id="comentarios"></textarea>
                        </div>
                    </div>


                </div>
            </div>

            <div class="col-md-5">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="form-group form-inline">
                            <div class="input-group">
                                <input type="text" class="form-control input-lg" style="width: 350px;" id="busqueda_query"  placeholder="Codigo de barras, Producto o Categoria">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-lg" id="buscar_producto" type="button">
                                        &nbsp; <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </span>
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-lg" id="btnlista" type="button">
                                        &nbsp; <i class="glyphicon glyphicon-list"></i>
                                    </button>
                                </span>
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-lg" id="btngrid" type="button">
                                        &nbsp; <i class="glyphicon glyphicon-th"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group form-inline" id="lista_breadcrumb">

                        </div>

                        <div id="lista_productos">
                            <table class="table table-borderless table-hover" >
                                <thead>
                                <tr class="bg-success-700">
                                    <th>Barcode</th>
                                    <th>Producto</th>
                                    <th>Precio Unitario</th>
                                    <th>En Stock</th>
                                    <th>Accion</th>
                                </tr>
                                </thead>
                                <tbody id="lista" >
                                <tr>
                                    <td style="vertical-align: text-top;">
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot style="text-align: right">

                                </tfoot>

                            </table>
                        </div>
                        <div id="grid_productos">

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    
    <div id="boleta_modal" class="modal modal-wide fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 id="bol_empresa"></h3>
                        </div>
                        <div class="col-sm-6 text-right" id="datos-recibo">
                            <b>FECHA:</b>
                            <span id="bol_fecha"></span><br>
                            <b>DIRECCION:</b>
                            <span id="bol_sucursal"></span><br>
                            <b>RUC</b>
                            <span id="bol_empresaRuc"></span><br>
                            <b>TELEFONO</b>
                            <span id="bol_empresaTelefono"></span><br>
                            <b>RECIBO</b>
                            <span id="bol_recibo"></span>
                        </div>

                    </div>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <div class="col-sm-6" id="datos_cliente">
                            <h5>CLIENTE:</h5>
                            <b>NOMBRES:</b>
                            <span id="bol_cliente"></span><br>
                            <b>DNI:</b>
                            <span id="bol_clienteDni"></span><br>
                            <b>RUC:</b>
                            <span id="bol_clienteRUC"></span><br>
                            <b>DIRECCION:</b>
                            <span id="bol_clienteDireccion"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table  class="table" id="validar_boleta">
                                <thead>
                                    <tr>
                                        <th>PRODUCTO</th>
                                        <th>PRECIO /U.</th>
                                        <th>CANTIDAD</th>
                                        <th>DESCUENTO</th>
                                        <th>IMPORTE</th>
                                    </tr>
                                </thead>
                                <tbody id="bol_detalle">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-9">
                            <table  class="table" id="validar_boleta">
                                <tr>
                                    <th>TOTAL</th>
                                    <td id="bol_total"></td>
                                </tr>
                                <tr>
                                    <th>PAGO</th>
                                    <td id="bol_pago"></td>
                                </tr>
                                <tr>
                                    <th>VUELTO</th>
                                    <td id="bol_vuelto"></td>
                                </tr>
                                <tr>
                                    <th>IGV</th>
                                    <td id="bol_igv"></td>
                                </tr>
                                <tr>
                                    <th>DESCUENTO</th>
                                    <td id="bol_descuento"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="row">

                            <div class="col-md-12">
                                <button class="btn btn-info btn-lg" id="btn_confirmar" data-idprod="9">
                                    <i class="glyphicon glyphicon-save"></i>
                                    Confirmar Transaccion
                                </button>
                            </div>

                        </div>
                    </div>



                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal fade" id="modal-info">
    	<div class="modal-dialog modal-lg">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    				<h4 class="modal-title" id="titulo-info">Informacion Producto</h4>
    			</div>
    			<div id="info-producto" class="modal-body">

    			</div>
    			<div class="modal-footer">
    				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

    			</div>
    		</div><!-- /.modal-content -->
    	</div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>

<script src="{{ URL::asset('/javascript/cal.js') }}" type="text/javascript"></script>

<script>

    $(document).ajaxSend(function(){
        $.LoadingOverlay("show", {
            fade  : [2000, 1000],
            zIndex          : 1500
        });
    });
    $(document).ajaxComplete(function(){
        $.LoadingOverlay("hide");
    });
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        var Lproductos = [];
        var precio_total = 0.00;

        var clientes = [];
        var cliente;
        var descuentos = [];
        $('#grid_productos').hide();

        $('#comentarios').wysihtml5({
            parserRules:  wysihtml5ParserRules,
            stylesheets: ["assets/css/components.css"],
            "image": false,
            "link": false,
            "font-styles": false,
            "emphasis": false
        });


        $('#busqueda_query').on('keydown', function(event) {
            if (event.which == 13 || event.keyCode == 13) {
                var query = $('#busqueda_query').val();
                var Lcategorias = [];
                $.get(currentLocation+"buscarProducto?query="+query+"", function( data ) {
                    var productos = jQuery.parseJSON( data );
                    var string = '';
                    Lproductos = [];
                    var card = '<div class="row" style="padding: 0 20px; ">';
                    var k = 0 ;
                    $.each(productos, function(key,value){
                        if(parseInt(value.cantidad) > 0){
                            Lproductos.push(value);

                            if(_.findWhere(Lcategorias, {idcategoria: value.idcategoria, descripcion : value.categoria , color:value.color }) == null){
                                Lcategorias.push({idcategoria: value.idcategoria, descripcion : value.categoria, color:value.color });
                            }

                            string += '<tr>';
                            string += '<td>'+value.barcode+'</td>';
                            string += '<td>'+value.nombre+'</td>';
                            string += '<td>S/.'+ parseFloat(value.precio).toFixed(2)+'</td>';
                            string += '<td>'+value.cantidad+'</td>';
                            string += '<td><button class="btn btn-info" id="btn_agregar" data-idprod ="'+ value.idproducto +'">Agregar</button></td></tr>';

                            var imagen = jQuery.parseJSON( value.imagenes );
                            var img ='' ;
                            if(imagen['array'][0].match('null')){
                                img = '/thumbs_180_140/productos_m_c.png';
                            }else{
                                img = imagen['array'][0];
                            }
                            if(value.tipo === 2) {
                                img = 'servicios.png';
                            }
                            card += '<div class="col-sm-12 col-md-6 col-lg-4">' +
                                '                <div class="card doctor" id="'+value.idproducto+'" >' +
                                '                    <div class="col-md-12 col-xs-12 text-center">' +
                                '                        <img  class="img-responsive" src="productos/'+ img  +'">' +
                                '                    </div>' +
                                '                    <div class="col-md-12 col-xs-12  text-center"  >' +
                                '                        <h2>'+value.nombre +'</h2>' +
                                '                        <p><b>Precio: S/</b> '+ parseFloat(value.precio).toFixed(2)+'</p>' +
                                '                        <p><b>Stock u.</b>'+value.cantidad+'</p>' +
                                '                    </div>' +
                                '                </div>' +
                                '        </div>';

                            k++;
                            if(k > 2){
                                card += "</div><div class='row' style='padding: 0 20px; '>";
                                k = 0;
                            }
                        }
                    });
                    $('#lista').html('').append(string);
                    $('#grid_productos').html('').append(card);
                    var lista_breadcrumb = '';
                    $.each(Lcategorias,function(key,value){
                        lista_breadcrumb += ' <button type="button" id="btn_categoria" style="background-color: '+ value.color +'" data-id='+value.idcategoria+' class="btn btn-lg bg-info-600">'+value.descripcion+'</button>';
                    });
                    $('#lista_breadcrumb').html('').append(lista_breadcrumb);
                });
            }
        });

        $('#buscar_producto').click(function(){
            var query = $('#busqueda_query').val();
            var Lcategorias = [];
            $.get(currentLocation+"buscarProducto?query="+query+"", function( data ) {
                var productos = jQuery.parseJSON( data );
                var string = '';
                Lproductos = [];
                var card = '<div class="row" style="padding: 0 20px; ">';
                var k = 0 ;
                $.each(productos, function(key,value){
                    if(parseInt(value.cantidad) > 0){
                        Lproductos.push(value);

                        if(_.findWhere(Lcategorias, {idcategoria: value.idcategoria, descripcion : value.categoria, color :value.color }) == null){
                            Lcategorias.push({idcategoria: value.idcategoria, descripcion : value.categoria, color:value.color });
                        }

                        string += '<tr>';
                        string += '<td>'+value.barcode+'</td>';
                        string += '<td>'+value.nombre+'</td>';
                        string += '<td>S/.'+ parseFloat(value.precio).toFixed(2)+'</td>';
                        string += '<td>'+value.cantidad+'</td>';
                        string += '<td><button class="btn btn-info" id="btn_agregar" data-idprod ="'+ value.idproducto +'">Agregar</button></td></tr>';

                        var imagen = jQuery.parseJSON( value.imagenes );
                        var img ='' ;
                        if(imagen['array'][0].match('null')){
                            img = '/thumbs_180_140/productos_m_c.png';
                        }else{
                            img = imagen['array'][0];
                        }
                        if(value.tipo === 2) {
                            img = 'servicios.png';
                        }
                        card += '<div class="col-sm-12 col-md-6 col-lg-4">' +
                            '                <div class="card doctor" id="'+value.idproducto+'" >' +
                            '                    <div class="col-md-12 col-xs-12 text-center">' +
                            '                        <img  class="img-responsive" src="productos/'+ img  +'">' +
                            '                    </div>' +
                            '                    <div class="col-md-12 col-xs-12  text-center"  >' +
                            '                        <h2>'+value.nombre +'</h2>' +
                            '                        <p><b>Precio: S/</b> '+ parseFloat(value.precio).toFixed(2)+'</p>' +
                            '                        <p><b>Stock u.</b>'+value.cantidad+'</p>' +
                            '                    </div>' +
                            '                </div>' +
                            '        </div>';

                        k++;
                        if(k > 2){
                            card += "</div><div class='row' style='padding: 0 20px; '>";
                            k = 0;
                        }
                    }
                });
                $('#lista').html('').append(string);
                $('#grid_productos').html('').append(card);
                var lista_breadcrumb = '';
                $.each(Lcategorias,function(key,value){
                    lista_breadcrumb += ' <button type="button" id="btn_categoria" data-id='+value.idcategoria+' style="background-color: '+ value.color +'"  class="btn btn-lg bg-info-600">'+value.descripcion+'</button>';
                });
                $('#lista_breadcrumb').html('').append(lista_breadcrumb);
            });


        });

        $('#lista_breadcrumb').on('click', '#btn_categoria', function(){
            var cat = $(this).data('id');
            //var cat_prod = _.where(Lproductos,{idcategotiria: cat});
            var cat_prod = [];
            for(var i =0 ; i < Lproductos.length ; i++){
                if(Lproductos[i].idcategoria === parseInt(cat)){
                    cat_prod.push(Lproductos[i]);
                }
            }
            var string = '';
            var card = '<div class="row" style="padding: 0 20px; ">';
            var k = 0;
            $.each(cat_prod, function(key,value){
                string += '<tr id='+value.idproducto +'>';
                string += '<td>'+value.barcode+'</td>';
                string += '<td>'+value.nombre+'</td>';
                string += '<td>S/.'+ parseFloat(value.precio).toFixed(2)+'</td>';
                string += '<td>'+value.cantidad+'</td>';
                string += '<td><button class="btn btn-info" id="btn_agregar" data-idprod ="'+ value.idproducto +'">Agregar</button></td></tr>';

                var imagen = jQuery.parseJSON( value.imagenes );
                var img ='' ;
                if(imagen['array'][0].match('null')){
                    img = '/thumbs_180_140/productos_m_c.png';
                }else{
                    img = imagen['array'][0];
                }
                if(value.tipo === 2) {
                    img = 'servicios.png';
                }
                card += '<div class="col-sm-12 col-md-6 col-lg-4">' +
                    '                <div class="card doctor" id="'+value.idproducto+'" >' +
                    '                    <div class="col-md-12 col-xs-12 text-center">' +
                    '                        <img  class="img-responsive" src="productos/'+ img  +'">' +
                    '                    </div>' +
                    '                    <div class="col-md-12 col-xs-12  text-center"  >' +
                    '                        <h2>'+value.nombre +'</h2>' +
                    '                        <p><b>Precio: S/</b> '+ parseFloat(value.precio).toFixed(2)+'</p>' +
                    '                        <p><b>Stock u.</b>'+value.cantidad+'</p>' +
                    '                    </div>' +
                    '                </div>' +
                    '        </div>';

                k++;
                if(k > 2){
                    card += "</div><div class='row' style='padding: 0 20px; '>";
                    k = 0;
                }

            });
            $('#lista').html('').append(string);
            $('#grid_productos').html('').append(card);
        });

        $('#lista').on('click','#btn_agregar',function(event){
            var idproducto =  $(this).data('idprod');
            var producto = _.find(Lproductos,{idproducto : idproducto });
            //carrito.push({idproducto: producto.idproducto, nombre: producto.nombre, cantidad:producto.cantidad, precio: producto.precio, descuento: 0.0, total_precio: 0.0});

            var string = '<tr id="tr_item" >';
            string += '<td id="detalle_cantidad"><input class="form-control" id="input_cantidad" data-idprod="'+ producto.idproducto +'" type="number" step="1" value="0"  ></td> ';

            string += '<td id="nombre_producto"><div id="ver" data-tipo="'+producto.tipo+'" data-idproducto="'+idproducto+'">'+producto.nombre+'</div></td>';
            string += '<td id="precio_unitario"><input class="form-control" type="text" id="precio_prod" value="'+ parseFloat(producto.precio).toFixed(2) +'"></td>';
            string += '<td id="detalle_descuento"><input class="form-control" id="input_descuento" data-idprod="'+ producto.idproducto +'" type="number" step="1" value="0"  ></td> ';
            string += '<td id="detalle_total"><input class="form-control" id="input_precio" type="number" step="0.1" value="0" disabled></td> ';
            string += '<td><button class="btn btn-danger btn-xs" id="eliminar" data-idprod="'+ producto.idproducto +'" ><i class="glyphicon glyphicon-remove"></i></button></td> ';

            string += '</tr> ';

            $('#lista_carrito').append(string);

        });

        $('#grid_productos').on('click','.row  .col-lg-4 .card', function(event){
            var idproducto = $(this).attr('id');
            var producto = _.find(Lproductos,{idproducto : parseInt(idproducto) });
            //carrito.push({idproducto: producto.idproducto, nombre: producto.nombre, cantidad:producto.cantidad, precio: producto.precio, descuento: 0.0, total_precio: 0.0});

            var string = '<tr id="tr_item">';
            string += '<td id="detalle_cantidad"><input class="form-control" id="input_cantidad" data-idprod="'+ producto.idproducto +'" type="number" step="1" value="0" ></td> ';

            string += '<td id="nombre_producto"><div id="ver" data-tipo="'+producto.tipo+'" data-idproducto="'+idproducto+'">'+producto.nombre+'</div></td>';
            string += '<td id="precio_unitario"><input class="form-control" type="text" id="precio_prod" value="'+ parseFloat(producto.precio).toFixed(2) +'"></td>';
            string += '<td id="detalle_descuento"><input class="form-control" id="input_descuento" data-idprod="'+ producto.idproducto +'" type="number" step="1" value="0"  ></td> ';
            string += '<td id="detalle_total"><input class="form-control" id="input_precio" type="number" step="0.1" value="0"disabled ></td> ';
            string += '<td><button class="btn btn-danger btn-xs" id="eliminar" data-idprod="'+ producto.idproducto +'" ><i class="glyphicon glyphicon-remove"></i></button></td> ';

            string += '</tr> ';

            $('#lista_carrito').append(string);


        });

        /********************CALCULAR TOTALES***********************************/

        function calcular_totales(){
            var total_detalle = 0;
            $("#lista_carrito #tr_item").each(function(){
                var importe = $(this).find('#detalle_total #input_precio');
                total_detalle += parseFloat(importe.val());
            });
            if ($('#con_igv').is(':checked')) {
                precio_total = (total_detalle);
                $('#total').val(precio_total ) ;

            }else{
                var igv = $('#igv').val();
                precio_total = (total_detalle + parseFloat(igv));
                $('#total').val(precio_total ) ;
            }
            calcular_descuento();
        }
        function calcular_igv(){
            var total_detalle = 0;
            $("#lista_carrito #tr_item").each(function(){
                var importe = $(this).find('#detalle_total #input_precio');
                total_detalle += parseFloat(importe.val());
            });
            var igv = total_detalle * 0.18;
            $('#igv').val(igv.toFixed(2));
        }

        $('#con_igv').on('change',function(event){
            var total_detalle = 0;
            $("#lista_carrito #tr_item").each(function(){
                var importe = $(this).find('#detalle_total #input_precio');
                total_detalle += parseFloat(importe.val());
            });
            if ($('#con_igv').is(':checked')) {
                precio_total = (total_detalle);
                $('#total').val(precio_total ) ;
            }else{
                var igv = $('#igv').val();
                precio_total = (total_detalle + parseFloat(igv));
                $('#total').val(precio_total ) ;
            }
            calcular_descuento()
        })


        function calcular_descuento(){
            var descuento = $('#descuento').val();
            $('#descuento').val( parseFloat(descuento).toFixed(2) );
            var precio = precio_total - descuento;
            $('#total').val(precio.toFixed(2));

            var paga = $('#paga').val();
            if(paga.length < 1)paga = '0';
            $(this).val( parseFloat(paga).toFixed(2));
            var total = $('#total').val();
            var vuelto = paga - total;
            if(paga.length !== 0){
                $('#vuelto').val(vuelto.toFixed(2));
            }
        }

        $('#descuento').on('change',function(event){
            var descuento = $('#descuento').val();
            $('#descuento').val( parseFloat(descuento).toFixed(2) );
            var precio = precio_total - descuento;
            $('#total').val(precio.toFixed(2));
        });

        /*********ELIMINAR Y LIMPIAR DE BOLETA ****************/

        $('#lista_carrito').on('click','#eliminar', function(event){
            $(this).closest('tr').remove();
            var idproducto = ($(this).parent().parent().find( "#nombre_producto #ver" )).data('idproducto') ;
            var descuento = _.findWhere(descuentos,{idproducto:idproducto});
            var pos = descuentos.indexOf(descuento);
            descuentos.splice(pos, 1);

            calcular_descuentos_arr();
            calcular_totales();
            calcular_igv();
        });

        $('#lista_carrito').on('click','#ver', function(event){
            var idproducto = $(this).data('idproducto');
            var tipo = $(this).data('tipo');

            if(tipo === 1){

                $.get('producto_info',{id:idproducto},function(data){
                    $('#titulo-info').html('').append('INFORMACION PRODUCTO');
                    $('#info-producto').append('').html(data);
                    $('#modal-info').modal();
                })

            }else{
                $.get('servicio_info',{id:idproducto},function(data){
                    $('#titulo-info').html('').append('INFORMACION SERVICIO');
                    $('#info-producto').append('').html(data);
                    $('#modal-info').modal();
                })
            }

        });

        $('#eliminar_all').click(function(event){
            $('#lista_carrito').html('');
            $('#total').val('0.00');
            $('#descuento').val('0.00');
            $('#igv').val('0.00');
            $('#paga').val('0.00');
            $('#vuelto').val('0.00');

        });

        /*******CANTIDAD Y DESCUENTOS INPUT CHANGE****************/

        $('#lista_carrito').on('keyup','#tr_item #input_cantidad', function(event){
            var precio = parseFloat( ($(this).parent().parent().find( "#precio_unitario #precio_prod" )).val());
            var total = $(this).parent().parent().find( "#detalle_total #input_precio" );
            var cantidad = parseFloat( $(this).val());
            var descuento = parseFloat( $('#lista_carrito #tr_item #input_descuento').val());
            descuento = descuento /100;
            var desc = (precio * cantidad) * descuento;
            var tprecio = (precio * cantidad).toFixed(2);

            var idproducto =  ($(this).parent().parent().find( "#nombre_producto #ver" )).data('idproducto') ;

            var desc_arr = _.findWhere(descuentos,{idproducto : idproducto });
            if(desc_arr == null){
                descuentos.push({idproducto:idproducto, descuento: desc});
            }else{
                desc_arr.descuento = desc;
            }

            total.val(tprecio);
            calcular_totales();
            calcular_igv();
        });

        /***************AGREGA DESCUENTOS AL ARRAY ****/
        $('#lista_carrito').on('keyup','#tr_item #input_descuento', function(event){
            var precio = parseFloat( ($(this).parent().parent().find( "#precio_unitario #precio_prod" )).val());
            var total = $(this).parent().parent().find( "#detalle_total #input_precio" );
            var cantidad = parseFloat( $('#lista_carrito #tr_item #input_cantidad').val());
            var descuento = parseFloat( $(this).val());
            descuento = descuento /100;
            var desc = (precio * cantidad) * descuento;
            var tprecio = (precio * cantidad).toFixed(2);
            /*var ant_descuento =  parseFloat($('#descuento').val());
            var total_des = (ant_descuento + desc);
            $('#descuento').val(total_des).change();
             total.val(tprecio);
            */
            var idproducto =  ($(this).parent().parent().find( "#nombre_producto #ver" )).data('idproducto') ;

            var desc_arr = _.findWhere(descuentos,{idproducto : idproducto });
            if(desc_arr == null){
                descuentos.push({idproducto:idproducto, descuento: desc});
            }else{
                desc_arr.descuento = desc;
            }

            calcular_descuentos_arr();
            calcular_totales();
            calcular_igv();
        });

        function calcular_descuentos_arr(){
            var suma = 0.00;
            $.each(descuentos,function(key, value){
               suma += parseFloat(value.descuento);
            });
            $('#descuento').val(suma);
        }

        $('#lista_carrito').on('change','#tr_item #precio_prod', function(event){
            var cantidad = parseFloat( ($(this).parent().parent().find( "#detalle_cantidad #input_cantidad" )).val());
            var total = $(this).parent().parent().find( "#detalle_total #input_precio" );
            var precio = parseFloat( $(this).val());
            $(this).val(precio.toFixed(2));
            var tprecio = (precio * cantidad).toFixed(2);
            total.val(tprecio);
            calcular_totales();
            calcular_igv();
        });


        $('#btnlista').click(function(event){
            $('#lista_productos').show();
            $('#grid_productos').hide();

        });

        $('#btngrid').click(function(event){
            $('#lista_productos').hide();
            $('#grid_productos').show();
        });

        /**************************BUSCAR CLIENTE*********/
        $('#cliente_search').change(function(event){
            if( $(this).val().length == 0){
                var temp;
                cliente = temp;
            }
        });

        $('#cliente_search').keyup(function(event) {
            var query = $(this).val();
            if(query.length >  4){
                $.get(currentLocation+"buscarCliente?query="+query+"", function( data ) {
                    $('#search_results').html('');
                    var obj = JSON.parse(data);
                    if(obj.length > 0){
                        $.each(obj, function(index, value) {
                            if(_.findWhere(clientes,{idcliente:value.idcliente}) == null){
                                clientes.push(value); }

                            $('#search_results')
                                .removeClass('hide')
                                .append("<div id='item_to_add' class='list-group-item' name='"
                                    +value.idcliente+"' >"+value.nombres +' ' + value.apellidos+'</div>');

                        });
                    }else{
                        $('#ruc').val('');
                        $('#nombres').val('');
                        $('#apellidos').val('');
                        $('#telefono').val('');
                        $('#direccion').val('');
                        cliente = null;
                    }

                });
            }else{
                $('#search_results').addClass('hide').html('');

            }
        });

        $('#item_to_add').hover(function() {
                $( this ).toggleClass( "active" );
            }, function() {
                $( this ).removeClass( "active" );
            }
        );

        $( "#search_results" ).on( "click","#item_to_add", function() {
            var idcliente = $(this).attr('name');
            cliente = _.findWhere(clientes, {idcliente: parseInt(idcliente)});
            $('#search_prod').val('');

            if(typeof cliente === "undefined" ){
                $('#ruc').val('');
                $('#nombres').val('');
                $('#apellidos').val('');
                $('#telefono').val('');
                $('#direccion').val('');
            }else{
                $('#cliente_search').val(cliente.dni);
                $('#ruc').val(cliente.ruc);
                $('#nombres').val(cliente.nombres);
                $('#apellidos').val(cliente.apellidos);
                $('#telefono').val(cliente.telefono);
                $('#direccion').val(cliente.direccion);
            }


            $('#search_results').addClass('hide').html('');

        });

        /*******************************************NUEVO CLIENTE**********************/
        $('#btn_nuevoCliente').click(function(event){
            window.open("nuevo_cliente", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,,200,width=800,height=600");
        });



        /********************************DOCUMENTOS*****************/
        $('#documento').on('change',function(event){
            var doc = $(this).val();
            if(doc === '2'){
                $('#ruc_group').show();
                swal({
                    title: "Tipo: FACTURA",
                    text: "Necesitas que el cliente tenga RUC registrado.",
                    confirmButtonColor: "#66BB6A",
                    type: "warning"
                },function(){
                    //window.location.reload();
                });
            }else{
                $('#ruc_group').hide();
            }

        });
        /***********************PAGA CON O VUELTO********/

        $('#total').change(function(event){
            var paga = $('#paga').val();
            var total = $(this).val();
            var vuelto = paga - total;
            if( paga){
                $('#vuelto').val(vuelto.toFixed(2));
            }

        });
        $('#paga').change(function(event){
            var paga = $(this).val();
            if(paga.length < 1)paga = '0';
            $(this).val( parseFloat(paga).toFixed(2));
            var total = $('#total').val();
            var vuelto = paga - total;
            if(paga.length !== 0){
                $('#vuelto').val(vuelto.toFixed(2));
            }
        });

        /****************************************************GUARDAR BOLETA****/

        $('#btn_guardar').click(function(event){
            var doc = $('#documento').val();


            if( $('#cliente_search').val() === ""){
                swal({
                    title: "Falta el campo Cliente",
                    text: "Debes ingresar un cliente.",
                    confirmButtonColor: "#66BB6A",
                    type: "error"
                },function(){
                    //window.location.reload();
                });
                return;
            }

            if( $('#ruc').val() === "" && doc === '2' ){
                swal({
                    title: "Falta el RUC del Cliente",
                    text: "Debes registar el ruc de "+ $('#nombres').val() + " " + $('#apellidos').val(),
                    confirmButtonColor: "#66BB6A",
                    type: "error"
                },function(){
                    //window.location.reload();
                });
                return;
            }

            var producto = [];
            $("#lista_carrito #tr_item").each(function(){
                var cantidad = $(this).find('#detalle_cantidad #input_cantidad').val();
                var idpro = $(this).find('#detalle_cantidad #input_cantidad').data('idprod');
                var nombre = $(this).find('#nombre_producto').html();
                var precio = $(this).find('#detalle_total #input_precio').val();
                producto.push({idproducto:idpro, nombre:nombre, cantidad:cantidad,precio:precio});
            });


            if(producto.length == 0){
                swal({
                    title: "Falta agregar productos o servicios",
                    text: "Debes escoger un producto o servicio.",
                    confirmButtonColor: "#66BB6A",
                    type: "error"
                },function(){
                    //window.location.reload();
                });
                return;
            }


            $('#bol_cliente').html('').append( $('#nombres').val() + " " + $('#apellidos').val() );
            $('#bol_clienteDireccion').html('').append( $('#direccion').val());
            $('#bol_clienteDni').html('').append( $('#cliente_search').val() );
            $('#bol_clienteRUC').html('').append( $('#ruc').val());

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd = '0'+dd
            }

            if(mm<10) {
                mm = '0'+mm
            }

            today = mm + '/' + dd + '/' + yyyy;

            $('#bol_fecha').html('').append(today);
            $('#bol_empresa').html('').append('{{ $usuario->empresa }}');
            $('#bol_sucursal').html('').append('{{ $usuario->direccion  }}');
            $('#bol_empresaRuc').html('').append('{{$usuario->ruc}}');
            $('#bol_empresaTelefono').html('').append('{{ $usuario->telefono }}');

            $('#bol_total').html('').append($('#total').val());
            $('#bol_igv').html('').append($('#igv').val());
            $('#bol_descuento').html('').append($('#descuento').val());
            $('#bol_pago').html('').append($('#paga').val());
            $('#bol_vuelto').html('').append($('#vuelto').val());

            var string = '';

            $("#lista_carrito #tr_item").each(function(){
                string += '<tr>';
                string += '<td>'+$(this).find('#nombre_producto #ver').html()+'</td>';
                string += '<td>'+$(this).find('#precio_unitario #precio_prod').val()+'</td>';
                string += '<td>'+$(this).find('#detalle_cantidad #input_cantidad').val()+'</td>';
                string += '<td>'+$(this).find('#detalle_total #input_precio').val()+'</td>';

                string += '</tr>';
            });
            console.log(string);
            $('#bol_detalle').html('').append(string);
            $('#bol_recibo').html('').append($('#documento :selected').html());
            $('#boleta_modal').modal();

        });



        $('#btn_confirmar').click(function(event){
            var igv = $('#igv').val();
            var descuento = $('#descuento').val();
            var total = $('#total').val();
            var comentarios = $('#comentarios').val();
            var productos = [];
            var paga = $('#paga').val();
            var vuelto =$('#vuelto').val();
            var tipo = $('#documento').val();

            $("#lista_carrito #tr_item").each(function(){
                var producto = $(this).find('#nombre_producto #ver').data('idproducto');
                var cantidad = $(this).find('#detalle_cantidad #input_cantidad').val();
                var precio = $(this).find('#detalle_total #input_precio').val();
                productos.push({idproducto:producto,cantidad:cantidad,precio:precio});
            });

            var json_prod = JSON.stringify(productos);

            console.log(cliente);
            if(cliente !== null){
                var idcliente = cliente.idcliente;
                $.post(currentLocation+'crear_caja',{idcliente:idcliente,igv:igv,descuento:descuento,total:total,comentarios:comentarios,productos:json_prod,paga:paga,vuelto:vuelto,tipo:tipo},function(data){
                    var obj = JSON.parse(data);
                    if(obj[0].created == 200){
                        window.open(currentLocation+"info_caja?id="+ obj[1].id,  "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,,200,width=800,height=600");
                        window.location.reload();

                    }

                });
            }else{
                var dni = $('#cliente_search').val();
                var ruc = $('#ruc').val();
                var nombres = $('#nombres').val();
                var apellidos = $('#apellidos').val();
                var telefono = $('#telefono').val();
                var direccion = $('#direccion').val();
                $.post(currentLocation+'guardar_cliente',{dni:dni, ruc:ruc, nombres:nombres, apellidos:apellidos,direccion:direccion,telefono:telefono,email:''},function(data){
                    var obj = JSON.parse(data);
                    if(obj.mensaje === 200){
                        var id = obj.idcliente;
                        $.post(currentLocation+'crear_caja',{idcliente:id,igv:igv,descuento:descuento,total:total,comentarios:comentarios,productos:json_prod,paga:paga,vuelto:vuelto,tipo:tipo},function(data){
                            var objCaja = JSON.parse(data);
                            if(objCaja[0].created == 200){
                                window.open(currentLocation+"info_caja?id="+ objCaja[1].id,  "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,,200,width=800,height=600");
                                window.location.reload();

                            }

                        });
                    }else{
                        swal({
                            title: "Error..!",
                            text: "No se puede guardar el cliente, intentalo de nuevo luego.",
                            confirmButtonColor: "#66BB6A",
                            type: "error"
                        },function(){

                        });
                        return;
                    }


                });

            }
        });





   </script>
@stop