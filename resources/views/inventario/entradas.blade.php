@extends('index')

<!-- TITULO PAGINA -->

@section('titulo')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Inventario / Entradas</span></h4>
@stop

<!--BREADCRUMB -->
@section('breadcrumb')
    <li><a href="/"><i class="icon-home2 position-left"></i> Home</a></li>
    <li>Inventario</li>
@stop
<!-- MENU AUXLIAR -->

@section('menu')

@stop

<!-- CONTENIDO DE LA PAGINA -->

@section('contenido')
    <?PHP
    header("Access-Control-Allow-Origin:*");
    ?>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">

    <div class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="form-group form-inline">
                            <div class="input-group pull-right">
                                <button type="button" class="btn btn-default btn-lg" id="eliminar_all" >
                                    <i class="glyphicon glyphicon-trash"></i>
                                    Limpiar
                                </button>
                                <button class="btn btn-info btn-lg" id="btn_guardar" data-idprod="9">
                                    <i class="glyphicon glyphicon-save"></i>
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <table  class="table table-borderless">
                            <thead>
                            <tr class="bg-indigo-600">
                                <th>Producto</th>
                                <th>En Stock</th>
                                <th>Cantidad</th>
                                <th>Accion</th>
                            </tr>
                            </thead>
                            <tbody id="lista_carrito" >
                            </tbody>
                            <tfoot style="text-align: right">
                            </tfoot>

                        </table>

                    </div>


                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="form-group form-inline">
                            <div class="input-group">
                                <input type="text" class="form-control input-lg" style="width: 400px;" id="busqueda_query"  placeholder="Buscar por codigo de barras, nombre o categoria">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-lg" id="buscar_producto" type="button">
                                        &nbsp; <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </span>
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-lg" id="btnlista" type="button">
                                        &nbsp; <i class="glyphicon glyphicon-list"></i>
                                    </button>
                                </span>
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-lg" id="btngrid" type="button">
                                        &nbsp; <i class="glyphicon glyphicon-th"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group form-inline" id="lista_breadcrumb">

                        </div>

                        <div id="lista_productos">
                            <table class="table table-borderless table-hover" >
                                <thead>
                                <tr class="bg-indigo-600">
                                    <th>Barcode</th>
                                    <th>Producto</th>
                                    <th>Precio Unitario</th>
                                    <th>En Stock</th>
                                    <th>Accion</th>
                                </tr>
                                </thead>
                                <tbody id="lista" >
                                <tr>
                                    <td style="vertical-align: text-top;">
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot style="text-align: right">

                                </tfoot>

                            </table>
                        </div>
                        <div id="grid_productos">

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

    <div class="modal fade" id="modal-info">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="titulo-info">Informacion Producto</h4>
                </div>
                <div id="info-producto" class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>

    <script src="{{ URL::asset('/javascript/cal.js') }}" type="text/javascript"></script>

    <script>
        window.onload = function () {
            calc.init();
        };
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        var Lproductos = [];
        $('#grid_productos').hide();




        $('#busqueda_query').on('keydown', function(event) {
            if (event.which == 13 || event.keyCode == 13) {
                var query = $('#busqueda_query').val();
                var Lcategorias = [];
                $.get(currentLocation+"buscarInventario?query="+query+"", function( data ) {
                    var productos = jQuery.parseJSON( data );
                    var string = '';
                    Lproductos = [];
                    var card = '<div class="row" style="padding: 0 20px; ">';
                    var k = 0 ;
                    $.each(productos, function(key,value){
                        if(parseInt(value.tipo) === 1 ){
                            Lproductos.push(value);

                            if(_.findWhere(Lcategorias, {idcategoria: value.idcategoria, descripcion : value.categoria , color:value.color }) == null){
                                Lcategorias.push({idcategoria: value.idcategoria, descripcion : value.categoria, color:value.color });
                            }

                            string += '<tr>';
                            string += '<td>'+value.barcode+'</td>';
                            string += '<td>'+value.nombre+'</td>';
                            string += '<td>S/.'+ parseFloat(value.precio).toFixed(2)+'</td>';
                            string += '<td>'+value.cantidad+'</td>';
                            string += '<td><button class="btn btn-info" id="btn_agregar" data-idprod ="'+ value.idproducto +'">Agregar</button></td></tr>';

                            var imagen = jQuery.parseJSON( value.imagenes );
                            var img ='' ;
                            if(imagen['array'][0].match('null')){
                                img = '/thumbs_180_140/productos_m_c.png';
                            }else{
                                img = imagen['array'][0];
                            }
                            if(value.tipo === 2) {
                                img = 'servicios.png';
                            }
                            card += '<div class="col-sm-12 col-md-6 col-lg-3">' +
                                '                <div class="card doctor" id="'+value.idproducto+'" >' +
                                '                    <div class="col-md-12 col-xs-12 text-center">' +
                                '                        <img  class="img-responsive" src="productos/'+ img  +'">' +
                                '                    </div>' +
                                '                    <div class="col-md-12 col-xs-12  text-center"  >' +
                                '                        <h2>'+value.nombre +'</h2>' +
                                '                        <p><b>Precio: S/</b> '+ parseFloat(value.precio).toFixed(2)+'</p>' +
                                '                        <p><b>Stock u.</b>'+value.cantidad+'</p>' +
                                '                    </div>' +
                                '                </div>' +
                                '        </div>';

                            k++;
                            if(k > 3){
                                card += "</div><div class='row' style='padding: 0 20px; '>";
                                k = 0;
                            }
                        }
                    });
                    $('#lista').html('').append(string);
                    $('#grid_productos').html('').append(card);
                    var lista_breadcrumb = '';
                    $.each(Lcategorias,function(key,value){
                        lista_breadcrumb += ' <button type="button" id="btn_categoria" style="background-color: '+ value.color +'" data-id='+value.idcategoria+' class="btn btn-lg bg-info-600">'+value.descripcion+'</button>';
                    });
                    $('#lista_breadcrumb').html('').append(lista_breadcrumb);
                });
            }
        });

        $('#buscar_producto').click(function(){
            var query = $('#busqueda_query').val();
            var Lcategorias = [];
            $.get(currentLocation+"buscarInventario?query="+query+"", function( data ) {
                var productos = jQuery.parseJSON( data );
                var string = '';
                Lproductos = [];
                var card = '<div class="row" style="padding: 0 20px; ">';
                var k = 0 ;
                $.each(productos, function(key,value){
                    if(parseInt(value.tipo) === 1 ){
                        Lproductos.push(value);

                        if(_.findWhere(Lcategorias, {idcategoria: value.idcategoria, descripcion : value.categoria, color :value.color }) == null){
                            Lcategorias.push({idcategoria: value.idcategoria, descripcion : value.categoria, color:value.color });
                        }

                        string += '<tr>';
                        string += '<td>'+value.barcode+'</td>';
                        string += '<td>'+value.nombre+'</td>';
                        string += '<td>S/.'+ parseFloat(value.precio).toFixed(2)+'</td>';
                        string += '<td>'+value.cantidad+'</td>';
                        string += '<td><button class="btn btn-info" id="btn_agregar" data-idprod ="'+ value.idproducto +'">Agregar</button></td></tr>';

                        var imagen = jQuery.parseJSON( value.imagenes );
                        var img ='' ;
                        if(imagen['array'][0].match('null')){
                            img = '/thumbs_180_140/productos_m_c.png';
                        }else{
                            img = imagen['array'][0];
                        }
                        if(value.tipo === 2) {
                            img = 'servicios.png';
                        }
                        card += '<div class="col-sm-12 col-md-6 col-lg-3">' +
                            '                <div class="card doctor" id="'+value.idproducto+'" >' +
                            '                    <div class="col-md-12 col-xs-12 text-center">' +
                            '                        <img  class="img-responsive" src="productos/'+ img  +'">' +
                            '                    </div>' +
                            '                    <div class="col-md-12 col-xs-12  text-center"  >' +
                            '                        <h2>'+value.nombre +'</h2>' +
                            '                        <p><b>Precio: S/</b> '+ parseFloat(value.precio).toFixed(2)+'</p>' +
                            '                        <p><b>Stock u.</b>'+value.cantidad+'</p>' +
                            '                    </div>' +
                            '                </div>' +
                            '        </div>';

                        k++;
                        if(k > 3){
                            card += "</div><div class='row' style='padding: 0 20px; '>";
                            k = 0;
                        }
                    }
                });
                $('#lista').html('').append(string);
                $('#grid_productos').html('').append(card);
                var lista_breadcrumb = '';
                $.each(Lcategorias,function(key,value){
                    lista_breadcrumb += ' <button type="button" id="btn_categoria" data-id='+value.idcategoria+' style="background-color: '+ value.color +'"  class="btn btn-lg bg-info-600">'+value.descripcion+'</button>';
                });
                $('#lista_breadcrumb').html('').append(lista_breadcrumb);
            });


        });

        $('#lista_breadcrumb').on('click', '#btn_categoria', function(){
            var cat = $(this).data('id');
            //var cat_prod = _.where(Lproductos,{idcategotiria: cat});
            var cat_prod = [];
            for(var i =0 ; i < Lproductos.length ; i++){
                if(Lproductos[i].idcategoria === parseInt(cat)){
                    cat_prod.push(Lproductos[i]);
                }
            }
            var string = '';
            var card = '<div class="row" style="padding: 0 20px; ">';
            var k = 0
            $.each(cat_prod, function(key,value){
                string += '<tr id='+value.idproducto +'>';
                string += '<td>'+value.barcode+'</td>';
                string += '<td>'+value.nombre+'</td>';
                string += '<td>S/.'+ parseFloat(value.precio).toFixed(2)+'</td>';
                string += '<td>'+value.cantidad+'</td>';
                string += '<td><button class="btn btn-info" id="btn_agregar" data-idprod ="'+ value.idproducto +'">Agregar</button></td></tr>';

                var imagen = jQuery.parseJSON( value.imagenes );
                var img ='' ;
                if(imagen['array'][0].match('null')){
                    img = '/thumbs_180_140/productos_m_c.png';
                }else{
                    img = imagen['array'][0];
                }
                if(value.tipo === 2) {
                    img = 'servicios.png';
                }
                card += '<div class="col-sm-12 col-md-6 col-lg-3">' +
                    '                <div class="card doctor" id="'+value.idproducto+'" >' +
                    '                    <div class="col-md-12 col-xs-12 text-center">' +
                    '                        <img  class="img-responsive" src="productos/'+ img  +'">' +
                    '                    </div>' +
                    '                    <div class="col-md-12 col-xs-12  text-center"  >' +
                    '                        <h2>'+value.nombre +'</h2>' +
                    '                        <p><b>Precio: S/</b> '+ parseFloat(value.precio).toFixed(2)+'</p>' +
                    '                        <p><b>Stock u.</b>'+value.cantidad+'</p>' +
                    '                    </div>' +
                    '                </div>' +
                    '        </div>';

                k++;
                if(k > 3){
                    card += "</div><div class='row' style='padding: 0 20px; '>";
                    k = 0;
                }

            });
            $('#lista').html('').append(string);
            $('#grid_productos').html('').append(card);
        });

        $('#lista').on('click','#btn_agregar',function(event){
            var idproducto =  $(this).data('idprod');
            var producto = _.find(Lproductos,{idproducto : idproducto });
            //carrito.push({idproducto: producto.idproducto, nombre: producto.nombre, cantidad:producto.cantidad, precio: producto.precio, descuento: 0.0, total_precio: 0.0});

            var string = '<tr id="tr_item" >';
            string += '<td id="nombre_producto"><div id="ver" data-tipo="'+producto.tipo+'" data-idproducto="'+idproducto+'">'+producto.nombre+'</div></td>';
            string += '<td id="detalle_total"><input class="form-control" id="input_stock" type="number" step="0.1" value="'+ producto.cantidad +'" disabled></td> ';
            string += '<td id="detalle_cantidad"><input class="form-control" id="input_cantidad" data-idprod="'+ producto.idproducto +'" type="number" step="1" value="0"  ></td> ';
            string += '<td><button class="btn btn-danger btn-xs" id="eliminar" data-idprod="'+ producto.idproducto +'" ><i class="glyphicon glyphicon-remove"></i></button></td> ';

            string += '</tr> ';

            $('#lista_carrito').append(string);

        });

        $('#grid_productos').on('click','.row  .col-lg-3 .card', function(event){
            var idproducto = $(this).attr('id');
            var producto = _.find(Lproductos,{idproducto : parseInt(idproducto) });
            //carrito.push({idproducto: producto.idproducto, nombre: producto.nombre, cantidad:producto.cantidad, precio: producto.precio, descuento: 0.0, total_precio: 0.0});

            var string = '<tr id="tr_item">';
            string += '<td id="nombre_producto"><div id="ver" data-tipo="'+producto.tipo+'" data-idproducto="'+idproducto+'">'+producto.nombre+'</div></td>';
            string += '<td id="detalle_total"><input class="form-control" id="input_stock" type="number" step="0.1" value="'+ producto.cantidad +'" disabled></td> ';
            string += '<td id="detalle_cantidad"><input class="form-control" id="input_cantidad" data-idprod="'+ producto.idproducto +'" type="number" step="1" value="0"  ></td> ';
            string += '<td><button class="btn btn-danger btn-xs" id="eliminar" data-idprod="'+ producto.idproducto +'" ><i class="glyphicon glyphicon-remove"></i></button></td> ';

            string += '</tr> ';

            $('#lista_carrito').append(string);


        });


        /*********ELIMINAR Y LIMPIAR DE BOLETA ****************/

        $('#lista_carrito').on('click','#eliminar', function(event){
            $(this).closest('tr').remove();
         });

        $('#lista_carrito').on('click','#ver', function(event){
            var idproducto = $(this).data('idproducto');
            var tipo = $(this).data('tipo');

            if(tipo === 1){

                $.get('producto_info',{id:idproducto},function(data){
                    $('#titulo-info').html('').append('INFORMACION PRODUCTO');
                    $('#info-producto').append('').html(data);
                    $('#modal-info').modal();
                })

            }else{
                $.get('servicio_info',{id:idproducto},function(data){
                    $('#titulo-info').html('').append('INFORMACION SERVICIO');
                    $('#info-producto').append('').html(data);
                    $('#modal-info').modal();
                })
            }

        });

        $('#eliminar_all').click(function(event){
            $('#lista_carrito').html('');

        });

        /*******CANTIDAD Y DESCUENTOS INPUT CHANGE****************/

        $('#lista_carrito').on('change','#tr_item #input_cantidad', function(event){

        });

        $('#btnlista').click(function(event){
            $('#lista_productos').show();
            $('#grid_productos').hide();

        });
//        FUNCIONDE BOTONES CAMBIO DE ESTILOS DIV PRODUCTOS
        $('#btngrid').click(function(event){
            $('#lista_productos').hide();
            $('#grid_productos').show();
        });

        $('#item_to_add').hover(function() {
                $( this ).toggleClass( "active" );
            }, function() {
                $( this ).removeClass( "active" );
            }
        );

        $( "#search_results" ).on( "click","#item_to_add", function() {
            $('#search_prod').val('');
            $('#search_results').addClass('hide').html('');

        });

        /****************************************************GUARDAR INVENTARIO****/

        $('#btn_guardar').click(function(event){

            var productos = [];
            $("#lista_carrito #tr_item").each(function(){
                var producto = $(this).find('#nombre_producto #ver').data('idproducto');
                var cantidad = $(this).find('#detalle_cantidad #input_cantidad').val();
                productos.push({idproducto:producto,cantidad:cantidad});
            });

            var json_prod = JSON.stringify(productos);

            $.post(currentLocation+'crear_entrada',{productos:json_prod},function(data){

                console.log(data);

                    swal({
                            title: "Bien hecho!",
                            text: "Se guardo correctamente",
                            type: "success"
                        },
                        function(){
                            console.log('ok button');
                            window.location.reload();
                        });


            });



        });





    </script>
@stop