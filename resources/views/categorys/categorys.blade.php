@extends('index')

<!-- TITULO PAGINA -->

@section('titulo')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Lista de Categorias</span></h4>
@stop

<!--BREADCRUMB -->
@section('breadcrumb')
    <li><a href="/"><i class="icon-home2 position-left"></i> Home</a></li>
    <li>Inventario</li>
    <li class="active">Categorias</li>
@stop
<!-- MENU AUXLIAR -->

@section('menu')

    <li>
        <a href="#" id="nuevo_producto">
            <i class="icon-box-add position-left"></i>
            Nuevo Categoria
        </a>

    </li>
@stop

<!-- CONTENIDO DE LA PAGINA -->
@section('contenido')
<?PHP
    header("Access-Control-Allow-Origin:*");
 ?>
<style type="text/css">
    .colorpicker-basic { z-index: 9999; }
    .sp-container { z-index: 9999 !important;}
    .sp-preview {z-index: 9999 !important;}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ URL::to('/') }}">

<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="heading-elements">



        </div>

    </div>

    <div class="panel-body">
        <!--LISTA DE PRODUCTOS -->
        <table class="table datatable-column-search-inputs dataTable table-hover dataTable no-footer" id="products_table">
            <thead>
            <tr>
                <th>Color</th>
                <th>Estado</th>
                <th>Descripcion</th>
                <th>Padre</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody id="categorias_table">
            @foreach ($categorias as $categoria)
                <tr>

                    <td  class="text-center" width="30px">
                        <div class="bg-green-800" style="background-color: {{ $categoria->color }}"><span>&nbsp</span></div>
                    </td>
                    <?PHP  if($categoria->state == 1) {
                        echo '<td><span class="label label-success">Activo</span></td>';
                    }else{
                        echo '<td><span class="label label-default">Inactivo</span></td>';
                    }  ?>
                    <td><h5>{{$categoria->descripcion}}</h5></td>
                    <td>{{$categoria->padre}}</td>
                    <td>
                        <button type="button" class="btn btn-info btn-xs"
                                data-idcategoria ={{$categoria->idcategoria}}
                                data-color="{{$categoria->color }}"
                                data-estado="{{$categoria->state}}"
                                data-descripcion="{{$categoria->descripcion}}"
                                data-padre="{{$categoria->idpadre}}"
                                id="editar" data-toggle="modal">
                            <i class="icon-pen6 position-left"></i> Editar
                        </button>
                        <button type="button" class="btn btn-danger btn-xs" data-idcategoria ={{$categoria->idcategoria}} id="eliminar">
                            <i class="icon-cancel-square2 position-left"></i> Eliminar
                        </button>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
    <div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
        <div class="text-right">
            {{ $categorias->links() }}
        </div>
    </div>

</div>

<div id="formulario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h6 class="modal-title">Modificar Categorias</h6>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="panel panel-flat">
                        <div class="panel-heading">

                        </div>

                        <div class="panel-body">
                            <fieldset>
                                <legend class="text-semibold">Informacion de Categoria</legend>

                                <div id="nombregroup" class="form-group">
                                    <label class="col-lg-3 control-label">Categoria:</label>
                                    <div class="col-lg-9">
                                        <input type="text"  id="descripcion" class="form-control" placeholder="Nombre de la Categoria">
                                        <input type="hidden"  id="idcategoria">
                                    </div>
                                </div>

                                <div id="categoriagroup" class="form-group">
                                    <label class="col-lg-3 control-label">Categoria Padre:</label>
                                    <div class="col-lg-9">
                                        <select id="padre" class="form-control">
                                            <option value="0">-- --</option>
                                            @foreach ($categorias as $category)
                                                <option value="{{ $category->idcategoria}}">{{$category->descripcion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Color de Categoria:</label>
                                    <div class="col-lg-9">
                                    <input type="text" class="form-control colorpicker-basic" id="color" data-cancel-text="Salir" data-choose-text="Escoger" value="#20BF7E">
                                    </div>
                                </div>

                            </fieldset>

                        </div>
                    </div>
                </div>

            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-info" id="guardar_cambios">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::asset('/javascript/categorys.js') }}" type="text/javascript"></script>
    @stop