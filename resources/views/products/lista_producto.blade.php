@extends('index')

<!-- TITULO PAGINA -->

@section('titulo')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Lista Productos</span></h4>
@stop

<!--BREADCRUMB -->
@section('breadcrumb')
    <li><a href="/"><i class="icon-home2 position-left"></i> Home</a></li>
    <li>Inventario</li>
    <li class="active">Lista de Productos</li>
@stop
<!-- MENU AUXLIAR -->

@section('menu')

    <li>
        <a href="producto_nuevo" target="_top" id="nuevo_producto">
            <i class="icon-box-add position-left"></i>
            Nuevo Producto
        </a>

    </li>
@stop

<!-- CONTENIDO DE LA PAGINA -->

@section('contenido')
    <?PHP
    header("Access-Control-Allow-Origin:*");
    ?>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">
    <style type="text/css">
        .imgbtn
        {
            border:none;
            background-color: #fff;
            border: dashed  #d2d2d2 1px;
            transition-duration: 0.4s;
            margin: 10px 5px !important;
            text-decoration: none;
        }
        .imgLoad {
            border:none;
            background-color: #fff;

        }


    </style>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>

    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="heading-elements">



            </div>

        </div>

        <div class="panel-body">
            <form class="form-inline">
                <div class="form-group">
                    <input type="text" id="busqueda_nombre" class="form-control input-lg" id="formGroupExampleInput" placeholder="Nombre del Producto">
                    <button type="button" id="btnbusqueda" class="btn btn-primary"><i class="icon-search4 position-left"></i> Bucar Nombre o Barcode</button>
                </div>
            </form>
            <div class="text-right">
                {{ $products->links() }}
            </div>
            <!--LISTA DE PRODUCTOS -->
            <table class="table datatable-column-search-inputs dataTable table-hover dataTable no-footer" id="products_table">
                <thead>
                <tr>
                    <th><i class="glyphicon glyphicon-picture"></i></th>
                    <th><i class="glyphicon glyphicon-barcode"></i></th>
                    <th>Categoria</th>
                    <th>Costo Unitario</th>
                    <th>Precio Unitario</th>
                    <th>Cantidad Total</th>
                    <th>Ubicacion</th>
                    <th>Procedencia</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody id="products">
                @foreach ($products as $product)
                    <div id="tr-prod">
                    <tr id="tr-acciones">
                        <?PHP $img = json_decode($product->imagenes);
                        $imagen = $img->array[0];
                        if($imagen == 'null'){
                            $imagen = 'productos_m_c.png';
                        }
                        echo '<td rowspan="2"><img width="150" src="productos/thumbs_180_140/'. $imagen .'"> </td>';
                        ?>
                        <td colspan="7" id="td-status"  style="background-color: rgba(230,234,238,0.5) "><h6 style=" display: inline-block;">{{$product->nombre}}</h6>
                            <br> <b>Creado: </b> {{ $product->created_at }}</td>
                            <?PHP  if($product->state == 1) {
                                echo '<td rowspan="2"><button id="status" class="btn btn-success" data-idproducto="'.$product->idproducto.'" data-status="0"><i class="glyphicon glyphicon-star"></i></button></td>';
                            }else{
                                echo '<td rowspan="2"><button id="status" class="btn btn-default" data-idproducto="'.$product->idproducto.'"  data-status="1"><i class="glyphicon glyphicon-star-empty"></i></button></td>';
                            }  ?>
                            <td rowspan="2">
                                <button type="button" class="btn btn-info btn-xs" id="editar"
                                        data-idproducto="{{$product->idproducto}}">
                                    <i class="icon-pen6 position-center"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-xs" data-idproducto="{{$product->idproducto}}"  id="eliminar">
                                    <i class="icon-cancel-square2 position-center"></i>
                                </button>
                            </td>
                    </tr>
                     <tr>
                         <td >{{$product->barcode}}</td>
                        <td>{{$product->categoria}}</td>
                        <td>S/. {{ round($product->costo,2)}}</td>
                        <td>S/. {{ round($product->precio,2)}}</td>
                        <td>u/ {{$product->cantidad}}</td>
                        <td>{{$product->ubicacion}}</td>
                        <td>{{$product->procedencia}}</td>

                    </tr>
                    </div>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
            <div class="text-right">
                {{ $products->links() }}
            </div>
        </div>

    </div>

    <script type="text/javascript" src="{{ asset('javascript/products.js') }}"></script>
    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        $('#btnbusqueda').click(function () {
           var query =  $('#busqueda_nombre').val();
           window.location.replace(currentLocation+'list_product?query='+query);
        });
    </script>


@stop
