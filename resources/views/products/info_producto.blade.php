<?PHP
header("Access-Control-Allow-Origin:*");
?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ URL::to('/') }}">
<style type="text/css">
    .imgbtn
    {
        border:none;
        background-color: #fff;
        border: dashed  #d2d2d2 1px;
        transition-duration: 0.4s;
        margin: 10px 5px !important;
        text-decoration: none;
    }
    .imgLoad {
        border:none;
        background-color: #fff;

    }
</style>

<script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>

<?php if($mensaje == 404)
{echo "ERROR";}else{
?>

<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="heading-elements">
        </div>

    </div>

    <div class="panel-body">
        <div class="form-horizontal">
            <div class="panel panel-flat">
                <div class="panel-heading text-right">
                    <div class="row">
                        <div class="col-md-12" style="font-weight: bolder; font-size: 18px">
                            <h2><div class="titleModal text-left" style="color:#00BCD4"></div></h2>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-7">
                                <div id="barcodegroup" class="form-group">
                                    <label class="col-lg-3 control-label"><i class="glyphicon glyphicon-barcode position-left "></i>BARCODE:</label>
                                    <div class="col-lg-9">
                                        <input type="text"  id="barcode" class="form-control" placeholder="barcode" value="{{$producto->barcode}}" disabled>
                                    </div>
                                </div>
                                <div id="nombregroup" class="form-group">
                                    <label class="col-lg-3 control-label">Nombre del Producto:</label>
                                    <div class="col-lg-9">
                                        <input type="text"  id="nombre" class="form-control" placeholder="Nombre del Producto" value="{{$producto->nombre}}" disabled>

                                    </div>
                                </div>
                                <div id="categoriagroup" class="form-group">
                                    <label class="col-lg-3 control-label">Categoria:</label>
                                    <div class="col-lg-9">
                                        <select id="categoria" class="form-control"  style="width: 100%;" disabled>
                                            @foreach ($categorias as $category)
                                                <option value="{{ $category->idcategoria}}">{{$category->descripcion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="costogroup" class="form-group">
                                    <label class="col-lg-3 control-label">Costo:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="costo" placeholder="000.00" value="{{$producto->costo}}" disabled>
                                    </div>
                                </div>

                                <div id="preciogroup" class="form-group">
                                    <label class="col-lg-3 control-label">Precio:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="precio" placeholder="000.00" value="{{$producto->precio}}" disabled>
                                    </div>
                                </div>

                                <div id="cantidadgroup" class="form-group">
                                    <label class="col-lg-3 control-label">Cantidad:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="cantidad" placeholder="u/" value="{{$producto->cantidad}}" disabled>
                                    </div>
                                </div>
                                <div id="fletegroup" class="form-group">
                                    <label class="col-lg-3 control-label">Precio Flete:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="flete" placeholder="S/" value="{{$producto->flete}}" >
                                    </div>
                                </div>
                                <div id="stockmingroup" class="form-group">
                                    <label class="col-lg-3 control-label">Stock Minimo:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="stock_min" placeholder="u/" value="{{$producto->stock_min}}" >
                                    </div>
                                </div>
                                <div id="marcagroup" class="form-group">
                                    <label class="col-lg-3 control-label">Marcas:</label>
                                    <div class="col-lg-9">
                                        <select id="marca" class="form-control"  style="width: 100%;" disabled>
                                            @foreach ($marcas as $marca)
                                                <option value="{{ $marca->idmarca}}">{{$marca->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div id="motosgroup" class="form-group">
                                    <label class="col-lg-3 control-label">Motos:</label>
                                    <div class="col-lg-9">
                                        <select id="motos" data-tags="true"  class="form-control" multiple="multiple" style="width: 100%;" disabled>
                                            @foreach ($motos as $moto)
                                                <option value="{{ $moto->idmoto}}">{{$moto->nombre}} - {{ $moto->serie }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div id="procedenciagroup" class="form-group">
                                    <label class="col-lg-3 control-label">Procedencia:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="procedencia" placeholder="Lugar de procedencia" value="{{$producto->procedencia}}" disabled>
                                    </div>
                                </div>

                                <div id="fabricantegroup" class="form-group">
                                    <label class="col-lg-3 control-label">Fabricante:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="fabricante" placeholder="Fabricante..." value="{{$producto->fabricante}}" disabled>
                                    </div>
                                </div>
                                <div id="proveedorgroup" class="form-group">
                                    <label class="col-lg-3 control-label">Proveedor:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="proveedor" placeholder="Proveedor..." value="{{$producto->proveedor}}" disabled>
                                    </div>
                                </div>
                                <div id="ubicaciongroup" class="form-group">
                                    <label class="col-lg-3 control-label">Ubicacion en Stock:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="ubicacion" placeholder="lugar en stock..." value="{{$producto->ubicacion}}" disabled>
                                    </div>
                                </div>
                                <div id="stategroup" class="form-group">
                                    <label class="col-lg-3 control-label"><i class="glyphicon glyphicon-star position-left" ></i>Estado Actual:</label>
                                    <div class="col-lg-9">
                                        <select id="state" class="form-control"  style="width: 100%;" disabled>
                                            <option value="0">Inactivado</option>
                                            <option value="1">Activo</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </fieldset>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="imagenesgroup" class="form-group">
                                <label class="col-lg-3 control-label">Imagenes</label>
                                <div class="col-lg-9">
                                    <button id="btnfile1" class="imgbtn" style="background-color: #00BCD4;padding: 10px" >
                                        <img height="100" id="img1" class="imgLoad">
                                    </button>


                                    <button id="btnfile2"  class="imgbtn">
                                        <img   height="100" id="img2" class="imgLoad">
                                    </button>

                                    <button id="btnfile3"  class="imgbtn">
                                        <img  height="100" id="img3" class="imgLoad" >
                                    </button>

                                    <button id="btnfile4"  class="imgbtn">
                                        <img  height="100" id="img4" class="imgLoad" >
                                    </button>

                                </div>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="descripciongroup" class="form-group">
                                <label class="col-lg-3 control-label">Descripcion:</label>
                                <div class="col-lg-9">
                                                    <textarea  id="descripcion"  class="wysihtml5 wysihtml5-min form-control" rows="50" cols="50" disabled>
                                                        {{ $producto->descripcion }}
                                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" rel="script">
     // Default initialization
    $('.wysihtml5').wysihtml5({
        parserRules:  wysihtml5ParserRules,
        stylesheets: ["assets/css/components.css"],
        "image": false,
        "link": false,
        "font-styles": false,
        "emphasis": false
    });
    $("#state").select2();
    $("#marca").select2();
    $('#categoria').select2();
    $('#motos').select2();


    var motos_arr = <?PHP  echo json_encode($motosProducto);   ?>;

    var motos = [];

    $.each(motos_arr,function(key,value){
        motos.push(value.idmoto);
    })

    $("#state").val({{ $producto->state}}).change();
    $("#marca").val({{ $producto->idmarca}}).change();
    $('#categoria').val({{ $producto->idcategoria }}).change();
    $('#motos').val(motos).change();


    $('#img1').attr('src', currentLocation+'images/imagen.png');
    $('#img2').attr('src', currentLocation+'images/imagen.png');
    $('#img3').attr('src', currentLocation+'images/imagen.png');
    $('#img4').attr('src', currentLocation+'images/imagen.png');

    var imagenes = JSON.parse(<?PHP echo json_encode($producto->imagenes)?>);
    var i = 1;
    $.each(imagenes.array,function(key,value){
        if(value === 'null'){
            return;
        }else{
            $('#img'+i).attr('src', 'productos/thumbs_180_140/'+value);
        }

        i++;
    });

    for(; i < 5; i++){
        $('#img'+i).attr('src', currentLocation+'images/imagen.png');
    }


</script>

<?PHP
}
?>
