@extends('index')

<!-- TITULO PAGINA -->

@section('titulo')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Lista Productos</span></h4>
@stop

<!--BREADCRUMB -->
@section('breadcrumb')
    <li><a href="/"><i class="icon-home2 position-left"></i> Home</a></li>
    <li>Inventario</li>
    <li class="active"><div class="titleModal text-left" style="color:#00BCD4"></div></li>
@stop
<!-- MENU AUXLIAR -->

@section('menu')

    <li>
        <div class="text-right">
            <button type="button" class="btn btn-info btn-lg" id="guardar_cambios">Guardar</button>
        </div>

    </li>
@stop

<!-- CONTENIDO DE LA PAGINA -->

@section('contenido')
    <?PHP
    header("Access-Control-Allow-Origin:*");
    ?>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">
    <style type="text/css">
        .imgbtn
        {
            border:none;
            background-color: #fff;
            border: dashed  #d2d2d2 1px;
            transition-duration: 0.4s;
            margin: 10px 5px !important;
            text-decoration: none;
        }
        .imgLoad {
            border:none;
            background-color: #fff;

        }
    </style>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>

    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="heading-elements">
            </div>

        </div>

        <div class="panel-body">
            <div class="form-horizontal">
                <div class="panel panel-flat">
                    <div class="panel-heading text-right">
                        <div class="row">
                            <div class="col-md-12" style="font-weight: bolder; font-size: 18px">
                                <h2><div class="titleModal text-left" style="color:#00BCD4"></div></h2>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <fieldset>
                            <legend class="text-semibold">Informacion del Producto</legend>

                            <div class="row">
                                <div class="col-md-7">
                                    <div id="barcodegroup" class="form-group">
                                        <label class="col-lg-3 control-label"><i class="glyphicon glyphicon-barcode position-left "></i>BARCODE:</label>
                                        <div class="col-lg-9">
                                            <input type="text"  id="barcode" class="form-control" placeholder="barcode">
                                        </div>
                                    </div>
                                    <div id="nombregroup" class="form-group">
                                        <label class="col-lg-3 control-label">Nombre del Producto:</label>
                                        <div class="col-lg-9">
                                            <input type="text"  id="nombre" class="form-control" placeholder="Nombre del Producto">
                                            <input type="hidden"  id="idproducto">
                                        </div>
                                    </div>
                                    <div id="categoriagroup" class="form-group">
                                        <label class="col-lg-3 control-label">Categoria:</label>
                                        <div class="col-lg-9">
                                            <select id="categoria" class="form-control"  style="width: 100%;">
                                                @foreach ($categorias as $category)
                                                    <option value="{{ $category->idcategoria}}">{{$category->descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div id="costogroup" class="form-group">
                                        <label class="col-lg-3 control-label">Costo:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="costo" placeholder="000.00">
                                        </div>
                                    </div>

                                    <div id="preciogroup" class="form-group">
                                        <label class="col-lg-3 control-label">Precio:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="precio" placeholder="000.00">
                                        </div>
                                    </div>

                                    <div id="cantidadgroup" class="form-group">
                                        <label class="col-lg-3 control-label">Cantidad:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="cantidad" placeholder="u/">
                                        </div>
                                    </div>
                                    <div id="fletegroup" class="form-group">
                                        <label class="col-lg-3 control-label">Precio Flete:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="flete" placeholder="S/" value="{{$producto->flete}}" >
                                        </div>
                                    </div>
                                    <div id="stockmingroup" class="form-group">
                                        <label class="col-lg-3 control-label">Stock Minimo:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="stock_min" placeholder="u/" value="{{$producto->stock_min}}" >
                                        </div>
                                    </div>
                                    <div id="marcagroup" class="form-group">
                                        <label class="col-lg-3 control-label">Marcas:</label>
                                        <div class="col-lg-9">
                                            <select id="marca" class="form-control"  style="width: 100%;" >
                                                @foreach ($marcas as $marca)
                                                    <option value="{{ $marca->idmarca}}">{{$marca->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div id="motosgroup" class="form-group">
                                        <label class="col-lg-3 control-label">Motos:</label>
                                        <div class="col-lg-9">
                                            <select id="motos" data-tags="true"  class="form-control" multiple="multiple" style="width: 100%;">
                                                @foreach ($motos as $moto)
                                                    <option value="{{ $moto->idmoto}}">{{$moto->nombre}} - {{ $moto->serie }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div id="procedenciagroup" class="form-group">
                                        <label class="col-lg-3 control-label">Procedencia:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="procedencia" placeholder="Lugar de procedencia">
                                        </div>
                                    </div>

                                    <div id="fabricantegroup" class="form-group">
                                        <label class="col-lg-3 control-label">Fabricante:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="fabricante" placeholder="Fabricante...">
                                        </div>
                                    </div>
                                    <div id="proveedorgroup" class="form-group">
                                        <label class="col-lg-3 control-label">Proveedor:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="proveedor" placeholder="Proveedor...">
                                        </div>
                                    </div>
                                    <div id="ubicaciongroup" class="form-group">
                                        <label class="col-lg-3 control-label">Ubicacion en Stock:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="ubicacion" placeholder="lugar en stock...">
                                        </div>
                                    </div>
                                    <div id="stategroup" class="form-group">
                                        <label class="col-lg-3 control-label"><i class="glyphicon glyphicon-star position-left" ></i>Estado Actual:</label>
                                        <div class="col-lg-9">
                                            <select id="state" class="form-control"  style="width: 100%;" >
                                                <option value="0">Inactivado</option>
                                                <option value="1" selected>Activo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </fieldset>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="imagenesgroup" class="form-group">
                                    <label class="col-lg-3 control-label">Imagenes</label>
                                    <div class="col-lg-9">
                                        <button id="btnfile1" class="imgbtn" style="background-color: #00BCD4;padding: 10px" >
                                            <img height="100" id="img1" class="imgLoad">
                                        </button>


                                        <button id="btnfile2"  class="imgbtn">
                                            <img   height="100" id="img2" class="imgLoad">
                                        </button>

                                        <button id="btnfile3"  class="imgbtn">
                                            <img  height="100" id="img3" class="imgLoad" >
                                        </button>

                                        <button id="btnfile4"  class="imgbtn">
                                            <img  height="100" id="img4" class="imgLoad" >
                                        </button>


                                        <input type="file" id="uploadfile1" class="hide" accept="image/*" data-type='image'>
                                        <input type="file" id="uploadfile2" class="hide" accept="image/*" data-type='image'>
                                        <input type="file" id="uploadfile3" class="hide" accept="image/*" data-type='image'>
                                        <input type="file" id="uploadfile4" class="hide" accept="image/*" data-type='image'>

                                        <div class="uploader">
                                            <span class="help-block" id="Inputmensaje">Solo formatos .jpg .png Maximo tamano del archivo es de  10Mb</span>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="descripciongroup" class="form-group">
                                    <label class="col-lg-3 control-label">Descripcion:</label>
                                    <div class="col-lg-9">
                                                    <textarea  id="descripcion"  class="wysihtml5 wysihtml5-min form-control" rows="50" cols="50">

                                                    </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" rel="script">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        $(document).ajaxSend(function(){
            $.LoadingOverlay("show", {
                fade  : [2000, 1000],
                zIndex          : 1500
            });
        });
        $(document).ajaxComplete(function(){
            $.LoadingOverlay("hide");
        });

        /*************BUSQUEDA DE PRODUCTOS POR NOMBRE******************/

        $('#btnbusqueda').on('click', function() {
            nombre = $('#busqueda_nombre').val();
            window.location.replace(currentLocation+"list_product?query="+nombre);
        });

        // Default initialization
        $('.wysihtml5').wysihtml5({
            parserRules:  wysihtml5ParserRules,
            stylesheets: ["assets/css/components.css"],
            "image": false,
            "link": false,
            "font-styles": false,
            "emphasis": false
        });

        $("#marca").select2();
        $('#categoria').select2();
        $('#motos').select2();

        var index = 0;

        /**********CARGA DE IMAGENES ********/
        $("#btnfile1").click(function () {
            index = 1;
            $("input#uploadfile1").click();
        });

        $("#btnfile2").click(function () {
            index = 2;
            $("input#uploadfile2").click();
        });

        $("#btnfile3").click(function () {
            index = 3;
            $("input#uploadfile3").click();
        });

        $("#btnfile4").click(function () {
            index = 4;
            $("input#uploadfile4").click();
        });



        $('#img1').attr('src', currentLocation+'images/imagen.png');
        $('#img2').attr('src', currentLocation+'images/imagen.png');
        $('#img3').attr('src', currentLocation+'images/imagen.png');
        $('#img4').attr('src', currentLocation+'images/imagen.png');

        $("#uploadfile1").change(function(e){
            $.LoadingOverlay("show");
            var size =  this.files[0].size/1024/1024 ;
            var nameImage = $(this).val();
            if(size > 20){
                $.LoadingOverlay("hide");
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >El archivo es demasiado grande, pesa '+ size.toFixed(2) +' MB y el limite es hasta 20 MB</span>');
                return;
            }
            if(nameImage.match(/jpg.*/)||nameImage.match(/jpeg.*/)||nameImage.match(/png.*/)||nameImage.match(/JPG.*/)||nameImage.match(/JPEG.*/)||nameImage.match(/PNG.*/)){
            }else{
                $.LoadingOverlay("hide");
                e.preventDefault();
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >Este formato no es valido</span>');
            }
            filesLenght = $(this)[0].files.length - 1;

            if (this.files && this.files[filesLenght]) {

                var FR= new FileReader();
                FR.addEventListener("load", function(e) {
                    $('#img'+ String(index) ).attr('src', e.target.result);
                });
                FR.readAsDataURL( this.files[filesLenght] );
                $.LoadingOverlay("hide");

            }else{
                $.LoadingOverlay("hide");
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >No es un archivo</span>');
            }


        });
        $("#uploadfile2").change(function(e){
            $.LoadingOverlay("show");
            var size =  this.files[0].size/1024/1024 ;
            var nameImage = $(this).val();
            if(size > 20){
                $.LoadingOverlay("hide");
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >El archivo es demasiado grande, pesa '+ size.toFixed(2) +' MB y el limite es hasta 20 MB</span>');
                return;
            }
            if(nameImage.match(/jpg.*/)||nameImage.match(/jpeg.*/)||nameImage.match(/png.*/)||nameImage.match(/JPG.*/)||nameImage.match(/JPEG.*/)||nameImage.match(/PNG.*/)){
            }else{
                $.LoadingOverlay("hide");
                e.preventDefault();
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >Este formato no es valido</span>');
            }
            filesLenght = $(this)[0].files.length - 1;

            if (this.files && this.files[filesLenght]) {

                var FR= new FileReader();
                FR.addEventListener("load", function(e) {
                    $('#img'+ String(index) ).attr('src', e.target.result);
                });
                FR.readAsDataURL( this.files[filesLenght] );
                $.LoadingOverlay("hide");

            }else{
                $.LoadingOverlay("hide");
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >No es un archivo</span>');
            }

        });
        $("#uploadfile3").change(function(e){
            $.LoadingOverlay("show");
            var size =  this.files[0].size/1024/1024 ;
            var nameImage = $(this).val();
            if(size > 20){
                $.LoadingOverlay("hide");
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >El archivo es demasiado grande, pesa '+ size.toFixed(2) +' MB y el limite es hasta 20 MB</span>');
                return;
            }
            if(nameImage.match(/jpg.*/)||nameImage.match(/jpeg.*/)||nameImage.match(/png.*/)||nameImage.match(/JPG.*/)||nameImage.match(/JPEG.*/)||nameImage.match(/PNG.*/)){
            }else{
                $.LoadingOverlay("hide");
                e.preventDefault();
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >Este formato no es valido</span>');
            }
            filesLenght = $(this)[0].files.length - 1;

            if (this.files && this.files[filesLenght]) {

                var FR= new FileReader();
                FR.addEventListener("load", function(e) {
                    $('#img'+ String(index) ).attr('src', e.target.result);
                });
                FR.readAsDataURL( this.files[filesLenght] );
                $.LoadingOverlay("hide");

            }else{
                $.LoadingOverlay("hide");
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >No es un archivo</span>');
            }

        });
        $("#uploadfile4").change(function(e){
            $.LoadingOverlay("show");
            var size =  this.files[0].size/1024/1024 ;
            var nameImage = $(this).val();
            if(size > 20){
                $.LoadingOverlay("hide");
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >El archivo es demasiado grande, pesa '+ size.toFixed(2) +' MB y el limite es hasta 20 MB</span>');
                return;
            }
            if(nameImage.match(/jpg.*/)||nameImage.match(/jpeg.*/)||nameImage.match(/png.*/)||nameImage.match(/JPG.*/)||nameImage.match(/JPEG.*/)||nameImage.match(/PNG.*/)){
            }else{
                $.LoadingOverlay("hide");
                e.preventDefault();
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >Este formato no es valido</span>');
            }
            filesLenght = $(this)[0].files.length - 1;

            if (this.files && this.files[filesLenght]) {

                var FR= new FileReader();
                FR.addEventListener("load", function(e) {
                    $('#img'+ String(index) ).attr('src', e.target.result);
                });
                FR.readAsDataURL( this.files[filesLenght] );
                $.LoadingOverlay("hide");

            }else{
                $.LoadingOverlay("hide");
                $('#Inputmensaje').html(' ')
                    .append('<span class="bg-warning" style="padding:15px; letter-spacing: 1px; word-spacing: 2px; " >No es un archivo</span>');
            }

        });


       $('#guardar_cambios').click(function(event){
           //idproducto = $('#idproducto').val();

           idproducto = 0;
           nombre = $('#nombre').val();
           categoria = $('#categoria').val();
           idmarca = $('#marca').val();
           cantidad = $('#cantidad').val();
           flete = $('#flete').val();
           stock_min = $('#stock_min').val();
           costo = $('#costo').val();
           precio = $('#precio').val();
           descripcion = $('#descripcion').val();
           var motos = $('#motos').val();
           var motos = $('#motos').val();
           procedencia = $('#procedencia').val();
           fabricante = $('#fabricante').val();
           proveedor = $('#proveedor').val();
           ubicacion = $('#ubicacion').val();
           estado = $('#state').val();
           barcode = $('#barcode').val();

           var formData = new FormData();
           var imagenes = [];
           for(i = 1; i < 5; i++){
             if( $("#uploadfile"+i)[0].files.length > 0 ){
                 if($("#uploadfile"+i)[0].files[0] !== 'undefined' &&  $("#uploadfile"+i)[0].files[0] !== null ){
                      imagenes.push(i);
                      formData.append('imagenes[]',$("#uploadfile"+i)[0].files[0]);
                 }
             }


           }

           formData.append('ids',imagenes);
           formData.append('idproducto',idproducto);
           formData.append("nombre",nombre);
           formData.append("categoria",categoria);
           formData.append("idmarca",idmarca);
           formData.append("cantidad",cantidad);
           formData.append("flete",flete);
           formData.append("stock_min",stock_min);
           formData.append("costo",costo);
           formData.append("precio",precio);
           formData.append("descripcion",descripcion);
           formData.append("motos",JSON.stringify(motos));
           formData.append("procedencia",procedencia);
           formData.append("fabricante",fabricante);
           formData.append("proveedor",proveedor);
           formData.append("ubicacion",ubicacion);
           formData.append("state",estado);
           formData.append("barcode",barcode);
           formData.append("tipo",1);


           if(validar_datos()){
               $.ajax({
                   url: currentLocation+"product_store", //You can replace this with MVC/WebAPI/PHP/Java etc
                   method: "post",
                   data: formData,
                   contentType: false,
                   async: true,//false, otherwise it creates a GET Request
                   processData: false,
                   beforeSend: function() {
                       $.LoadingOverlay("show");
                   },
                   complete: function(){
                       $.LoadingOverlay("hide");
                   },
                   success: function () {

                       swal({
                               title: "Bien hecho!",
                               text: "Se guardo correctamente",
                               type: "success"
                           },
                           function(){
                               window.location = "{{ url('/list_product')}}";
                           });
                       ;
                   },
                   error: function (error) {
                       $.LoadingOverlay("hide");
                       swal("Error al guardar", "Intentelo nuevamente luego.", "error"); }

               });
           }
       });


       function validar_datos(){
            nombre =  $('#nombre').val();


            if(nombre  === undefined || nombre === ''){
                $('#nombregroup').addClass("has-error");
                return false;
            }


            return true;

        }


    </script>

@stop