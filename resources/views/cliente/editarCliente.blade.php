@extends('index')

<!-- TITULO PAGINA -->

@section('titulo')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Nuevo Cliente</span></h4>
@stop

<!--BREADCRUMB -->
@section('breadcrumb')
    <li><a href="/"><i class="icon-home2 position-left"></i> Home</a></li>
    <li>Cliente</li>
    <li class="active">Nuevo Cliente</li>
@stop
<!-- MENU AUXLIAR -->

@section('menu')

    <li>
        <div class="from-group pull-right">
            <button class="btn btn-success btn-lg" id="btn_guardar" data-idprod="9">
                <i class="glyphicon glyphicon-save"></i>
                Guardar
            </button>
        </div>

    </li>
@stop

<!-- CONTENIDO DE LA PAGINA -->
@section('contenido')
    <?PHP
    header("Access-Control-Allow-Origin:*");
    ?>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">


    <div class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="from-group" id="dni_group">
                        <label for="dni">*DNI:</label>
                        <input type="text" class="form-control" id="dni" value="{{ $cliente->dni }}">
                        <input type="hidden" class="form-control" id="idcliente" value="{{ $cliente->idcliente }}">
                    </div>
                    <div class="from-group" id="ruc_group">
                        <label for="ruc">RUC:</label>
                        <input type="text" class="form-control" id="ruc" value="{{ $cliente->ruc }}">
                    </div>
                    <div class="from-group" id="nombre_group">
                        <label for="nombres">*Nombres:</label>
                        <input type="text" class="form-control" id="nombres" placeholder="Nombres" value="{{ $cliente->nombres }}">
                    </div>
                    <div class="from-group" id="apellido_group">
                        <label for="apellidos">*Apellidos:</label>
                        <input type="text" class="form-control" id="apellidos" placeholder="Apellidos." value="{{ $cliente->apellidos }}">
                    </div>
                    <div class="from-group" id="telf_group">
                        <label for="telefono">Telf.:</label>
                        <input type="text" class="form-control" id="telefono" value="{{ $cliente->telefono }}">
                    </div>
                    <div class="from-group" id="direccion_group">
                        <label for="direccion">Direccion:</label>
                        <input type="text" class="form-control" id="direccion" value="{{ $cliente->direccion }}">
                    </div>
                    <div class="from-group" id="email_group">
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" value="{{ $cliente->email }}">
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

    <script rel="script" type="text/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';

        $('#btn_guardar').click(function(event){
            var idcliente = $('#idcliente').val();
            var dni = $('#dni').val();
            var ruc = $('#ruc').val();
            var nombres = $('#nombres').val();
            var apellidos = $('#apellidos').val();
            var direccion = $('#direccion').val();
            var email = $('#email').val();
            var telefono = $('#telefono').val();

            console.log(idcliente);

            if(dni.length == 0){
                swal({
                    title: "Upss!",
                    text: "Debes agregar el DNI del cliente",
                    confirmButtonColor: "#66BB6A",
                    type: "error"
                },function(){
                    //window.location.reload();
                });
                return;
            }
            if(nombres.length == 0){
                swal({
                    title: "Upss!",
                    text: "Debes agregar los nombres del Cliente",
                    confirmButtonColor: "#66BB6A",
                    type: "error"
                },function(){
                    //window.location.reload();
                });
                return;
            }
            if(apellidos.length == 0){
                swal({
                    title: "Upss!",
                    text: "Debes agregar los apellidos del cliente",
                    confirmButtonColor: "#66BB6A",
                    type: "error"
                },function(){
                    //window.location.reload();
                });
                return;
            }

            console.log(idcliente);
            $.post(currentLocation+'guardar_cliente',{idcliente:idcliente, dni:dni, ruc:ruc, nombres:nombres, apellidos:apellidos,direccion:direccion,telefono:telefono,email:email},function(data){
                obj = JSON.parse(data);
                if(obj.mensaje === 200){
                    swal({
                        title: "Ok!",
                        text: "Se guardo correctamente!.",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                    },function(){
                        window.opener.location.reload();
                        window.close();
                    });
                    return;
                }else{
                    swal({
                        title: "Error..!",
                        text: "No se puede guardar el cliente, intentalo de nuevo luego.",
                        confirmButtonColor: "#66BB6A",
                        type: "error"
                    },function(){
                    });
                    return;
                }
            });

        });

    </script>
@stop