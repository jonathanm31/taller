@extends('index')

<!-- TITULO PAGINA -->

@section('titulo')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Lista de clientes</span></h4>
@stop

<!--BREADCRUMB -->
@section('breadcrumb')
    <li><a href="/"><i class="icon-home2 position-left"></i> Home</a></li>
    <li>Inventario</li>
    <li class="active">clientes</li>
@stop
<!-- MENU AUXLIAR -->

@section('menu')

    <li>
        <a href="nuevo_cliente" target="_top" id="nuevo_producto">
            <i class="icon-box-add position-left"></i>
            Nuevo cliente
        </a>

    </li>
@stop

<!-- CONTENIDO DE LA PAGINA -->
@section('contenido')
    <?PHP
    header("Access-Control-Allow-Origin:*");
    ?>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">

    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="heading-elements">
            </div>

        </div>

        <div class="panel-body">

            <!--LISTA DE PRODUCTOS -->
            <div class="text-right">
                {{ $clientes->links() }}
            </div>
            <table class="table datatable-column-search-inputs dataTable table-hover dataTable no-footer" id="cliente_table">
                <thead>
                <tr>
                    <th>Estado</th>
                    <th>DNI</th>
                    <th>RUC</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Direccion</th>
                    <th>Telefono</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody id="cliente_table">
                @foreach ($clientes as $cliente)
                    <tr id="tr_detalle">
                        <?PHP  if($cliente->state == 1) {
                            echo '<td><span class="label label-success">Activo</span></td>';
                        }else{
                            echo '<td><span class="label label-default">Inactivo</span></td>';
                        }  ?>
                        <td>{{ $cliente->dni }}</td>
                        <td>{{ $cliente->ruc }}</td>
                        <td>{{$cliente->nombres}}</td>
                        <td><h5>{{$cliente->apellidos}}</h5></td>
                        <td>{{$cliente->direccion}}</td>
                        <td>{{$cliente->telefono}}</td>

                        <td id="td_actions">
                            <button type="button" class="btn btn-info btn-xs"
                                    id="editar" data-idcliente="{{$cliente->idcliente}}">
                                <i class="icon-pen6 position-left"></i> Editar
                            </button>
                            <button type="button" class="btn btn-danger btn-xs" id="eliminar"
                                    data-idcliente="{{$cliente->idcliente}}">
                                <i class="icon-cancel-square2 position-left"></i> Eliminar
                            </button>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
            <div class="text-right">

            </div>
        </div>

    </div>


    <script type="application/javascript" rel="script">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';

        $('#cliente_table').on('click','#tr_detalle #td_actions #editar',function(events){
            var idcliente = $(this).data('idcliente');
            window.open(currentLocation+"editar_cliente?idcliente="+idcliente, "_top", "toolbar=yes,scrollbars=yes,resizable=yes,,200,width=800,height=600");

        });
        $('#cliente_table').on('click','#tr_detalle #td_actions #eliminar',function(events){
            var idcliente = $(this).data('idcliente');

            swal({
                    title: "Estas seguro?",
                    text: "No podras recuperar este cliente si lo eliminas!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "Si, eliminar!",
                    cancelButtonText: "No, salir",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm) {
                        var jqxhr =  $.get(currentLocation+"eliminar_cliente?idcliente="+idcliente, function(data,status){

                        }).done(function() {
                            swal({
                                title: "Eliminado!",
                                text: "El cliente ha sido eliminado.",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            },function(){
                                window.location.reload();
                            });
                        }).fail(function() {
                            swal("Error al eliminar al cliente", "Intentelo nuevamente luego.", "error");
                        });

                    }
                    else {
                        swal({
                            title: "Cancelado",
                            text: "No se ha eliminado nada :)",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
        });

    </script>
@stop