@extends('index')

<!-- TITULO PAGINA -->

@section('titulo')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Editar Marca {{ $marca->nombre }}  &nbsp; </span><img height="40" src="marcas/{{ $marca->img }}" alt=""></h4>

@stop

<!--BREADCRUMB -->
@section('breadcrumb')
    <li><a href="/"><i class="icon-home2 position-left"></i> Home</a></li>
    <li>Marcas</li>
    <li>Nueva Marca</li>
    <li class="active"><div class="titleModal text-left" style="color:#00BCD4"></div></li>
@stop
<!-- MENU AUXLIAR -->

@section('menu')

    <li>
        <div class="text-right">
            <button type="button" class="btn btn-info btn-lg" id="guardar_cambios">Guardar</button>
        </div>

    </li>
@stop

<!-- CONTENIDO DE LA PAGINA -->

@section('contenido')
    <?PHP
    header("Access-Control-Allow-Origin:*");
    ?>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">

    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>

    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="heading-elements">
            </div>

        </div>

        <div class="panel-body">
            <div class="form-horizontal">
                <div class="panel panel-flat">
                    <div class="panel-heading text-right">
                        <div class="row">
                            <div class="col-md-12" style="font-weight: bolder; font-size: 18px">
                                <h2><div class="titleModal text-left" style="color:#00BCD4"></div></h2>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <fieldset>
                            <legend class="text-semibold">Informacion de marca</legend>

                            <div id="nombregroup" class="form-group">
                                <label class="col-lg-3 control-label">Marca:</label>
                                <div class="col-lg-9">
                                    <input type="text"  id="nombre" class="form-control" placeholder="Nombre de la Marca" value="{{ $marca->nombre }}">
                                    <input type="hidden"  id="idmarca" value="{{ $marca->idmarca }}">
                                </div>
                            </div>

                            <div id="descripciongroup" class="form-group">
                                <label class="col-lg-3 control-label">Descripcion:</label>
                                <div class="col-lg-9">
                                    <input type="text"  id="descripcion" value="{{ $marca->descripcion }}" class="form-control" placeholder="Descripcion de la Marca">
                                </div>
                            </div>

                            <div id="imagengroup" class="form-group">
                                <label class="col-lg-3 control-label">Imagen:</label>
                                <div class="col-lg-9">
                                    <input type="file" name="imagen" id="imagen" class="form-control" placeholder="Archivo de imagen .jpg .png">

                                </div>
                                <img id="img" src="" height="100">
                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';

        $('.wysihtml5').wysihtml5({
            parserRules:  wysihtml5ParserRules,
            stylesheets: ["assets/css/components.css"],
            "image": false,
            "link": false,
            "font-styles": false,
            "emphasis": false
        });

        $('#guardar_cambios').click(function(e){
            event.preventDefault();
            idmarca = $('#idmarca').val();
            descripcion = $('#descripcion').val();
            nombre = $('#nombre').val();
            var imagen = $("#imagen")[0].files;

            var formData = new FormData();

            formData.append('imagen', imagen[0]);
            formData.append('idmarca',idmarca);
            formData.append("descripcion",descripcion);
            formData.append("nombre",nombre);
            formData.append("edit",1);

            if(validar_datos()){
                $.ajax({
                    url: currentLocation+"marca_store", //You can replace this with MVC/WebAPI/PHP/Java etc
                    method: "post",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function () {
                        $('#formulario').modal('hide');

                        swal({
                                title: "Bien hecho!",
                                text: "Se guardo correctamente",
                                type: "success"
                            },
                            function(){
                                window.location = "{{ url('/list_marcas')}}";
                            });
                    },
                    error: function (error) { swal("Error al guardar", "Intentelo nuevamente luego.", "error"); }

                });
            }

        });

        function validar_datos(){
            nombre =  $('#nombre').val();
            if(nombre  === undefined || nombre === ''){
                $('#nombregroup').addClass("has-error");
                return false;
            }


            return true;

        }
    </script>
@stop