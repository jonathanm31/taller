@extends('index')

<!-- TITULO PAGINA -->

@section('titulo')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Lista de Marcas</span></h4>
@stop

<!--BREADCRUMB -->
@section('breadcrumb')
    <li><a href="/"><i class="icon-home2 position-left"></i> Home</a></li>
    <li>Inventario</li>
    <li class="active">Marcas</li>
@stop
<!-- MENU AUXLIAR -->

@section('menu')

    <li>
        <a href="nueva_marca" id="nuevo_producto">
            <i class="icon-box-add position-left"></i>
            Nuevo marca
        </a>

    </li>
@stop

<!-- CONTENIDO DE LA PAGINA -->
@section('contenido')
    <?PHP
    header("Access-Control-Allow-Origin:*");
    ?>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">

    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="heading-elements">



            </div>

        </div>

        <div class="panel-body">
            <form class="form-inline">
                <input type="text" id="busqueda_nombre" class="form-control input-lg" id="formGroupExampleInput" placeholder="Nombre de Marca">
                <button type="button" id="btnbusqueda" class="btn btn-primary"><i class="icon-search4 position-left"></i> Buscar</button>
            </form>
            <!--LISTA DE PRODUCTOS -->
            <table class="table datatable-column-search-inputs dataTable table-hover dataTable no-footer" id="marca_table">
                <thead>
                <tr>
                    <th>Estado</th>
                    <th>Logo</th>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody id="marca_table">
                @foreach ($marcas as $marca)
                    <tr id="lista_marcas">
                        <?PHP  if($marca->state == 1) {
                            echo '<td><span class="label label-success">Activo</span></td>';
                        }else{
                            echo '<td><span class="label label-default">Inactivo</span></td>';
                        }  ?>
                        <td class="text-center"><img src="marcas/{{ $marca->img }}" height="50" ></td>
                        <td><h4>{{$marca->nombre}}</h4></td>
                        <td><p>{{$marca->descripcion}}</p></td>

                        <td>
                            <a class="btn btn-info btn-xs" href="editar_marca?idmarca={{$marca->idmarca}}">
                                <i class="icon-pen6 position-left"></i> Editar
                            </a>
                            <button type="button" class="btn btn-danger btn-xs" data-idmarca="{{$marca->idmarca}}" id="eliminar">
                                <i class="icon-cancel-square2 position-left"></i> Eliminar
                            </button>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
            <div class="text-right">
                {{ $marcas->links() }}
        </div>
        </div>

    </div>


    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';

        $('#marca_table').on('click','#lista_marcas #eliminar' ,function() {
            idmarca=  $(this).data('idmarca');

            swal({
                    title: "Estas seguro?",
                    text: "No podras recuperar este producto si lo eliminas!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "Si, eliminar!",
                    cancelButtonText: "No, salir",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm) {
                        var jqxhr =  $.post(currentLocation+"marca_delete",{idmarca:idmarca},function(data,status){

                        }).done(function() {
                            swal({
                                title: "Eliminado!",
                                text: "La Marca ha sido eliminado.",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            },function(){
                                window.location = "{{ url('/list_marcas')}}";
                            });
                        }).fail(function() {
                            swal("Error al eliminar marca", "Intentelo nuevamente luego.", "error");
                        });
                    }
                    else {
                        swal({
                            title: "Cancelado",
                            text: "No se ha eliminado nada :)",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
        });
        $('#btnbusqueda').click(function(){
            var query = $('#busqueda_nombre').val();
            var link = "?query="+query;
            window.location = "{{ url('/list_marcas')}}"+link;
        });


    </script>
@stop