@extends('index')

<!-- TITULO PAGINA -->

@section('titulo')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Lista de motos</span></h4>
@stop

<!--BREADCRUMB -->
@section('breadcrumb')
    <li><a href="/"><i class="icon-home2 position-left"></i> Home</a></li>
    <li>Inventario</li>
    <li class="active">Motos</li>
@stop
<!-- MENU AUXLIAR -->

@section('menu')

    <li>
        <a href="nueva_moto" id="">
            <i class="icon-box-add position-left"></i>
            Nuevo moto
        </a>

    </li>
@stop

<!-- CONTENIDO DE LA PAGINA -->
@section('contenido')
    <?PHP
    header("Access-Control-Allow-Origin:*");
    ?>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">

    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="heading-elements">



            </div>

        </div>

        <div class="panel-body">
            <form class="form-inline">
                <input type="text" id="busqueda_nombre" class="form-control input-lg" id="formGroupExampleInput" placeholder="Nombre de la moto">
                <button type="button" id="btnbusqueda" class="btn btn-primary"><i class="icon-search4 position-left"></i> Buscar</button>
            </form>
            <!--LISTA DE PRODUCTOS -->
            <table class="table datatable-column-search-inputs dataTable table-hover dataTable no-footer" id="moto_table">
                <thead>
                <tr>
                    <th>Estado</th>
                    <th></th>
                    <th>Nombre</th>
                    <th>Marca</th>
                    <th>Serie</th>
                    <th>Descripcion</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody id="moto_table">
                @foreach ($motos as $moto)
                    <tr>
                        <?PHP  if($moto->state == 1) {
                            echo '<td><span class="label label-success">Activo</span></td>';
                        }else{
                            echo '<td><span class="label label-default">Inactivo</span></td>';
                        }  ?>
                        <td><img src="motos/{{ $moto->imagen }}"  width="150" ></td>
                        <td>{{$moto->nombre}}</td>
                        <td>{{$moto->marca}}</td>
                        <td>{{$moto->serie}}</td>
                        <td><?PHP echo substr($moto->descripcion,0,50).' ...' ?></td>

                        <td>
                            <a class="btn btn-info btn-xs" href="editar_moto?idmoto={{$moto->idmoto}}"> <i class="icon-pen6 position-left"></i> Editar</a>

                            <button type="button" class="btn btn-danger btn-xs" data-idmoto="{{$moto->idmoto}}" id="eliminar">
                                <i class="icon-cancel-square2 position-left"></i> Eliminar
                            </button>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
            <div class="text-right">
                {{ $motos->links() }}
            </div>
        </div>

    </div>

    <div id="formulario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h6 class="modal-title">Modificar moto</h6>
                </div>

                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="panel panel-flat">
                            <div class="panel-heading">

                            </div>

                            <div class="panel-body">
                                <fieldset>
                                    <legend class="text-semibold">Informacion de moto</legend>

                                    <div id="nombregroup" class="form-group">
                                        <label class="col-lg-3 control-label">Nombre:</label>
                                        <div class="col-lg-9">
                                            <input type="text"  id="nombre" class="form-control" placeholder="Nombre de la moto">
                                            <input type="hidden"  id="idmoto">
                                        </div>
                                    </div>

                                    <div id="descripciongroup" class="form-group">
                                        <label class="col-lg-3 control-label">Descripcion:</label>
                                        <div class="col-lg-9">
                                            <textarea  id="descripcion"  class="wysihtml5 wysihtml5-min form-control" rows="50" cols="50">
                                            </textarea>
                                        </div>
                                    </div>
                                    <div id="seriegroup" class="form-group">
                                        <label class="col-lg-3 control-label">Serie:</label>
                                        <div class="col-lg-9">
                                            <input type="text"  id="serie" class="form-control" placeholder="Serie">
                                        </div>
                                    </div>
                                    <div id="marcagroup" class="form-group">
                                        <label class="col-lg-3 control-label">Marca:</label>
                                        <div class="col-lg-9">
                                            <select id="idmarca" class="form-control" placeholder="Insertar Marca" >
                                                @foreach ($marcas as $marca)
                                                    <option value="{{ $marca->idmarca }}">{{ $marca->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div id="imagengroup" class="form-group">
                                        <label class="col-lg-3 control-label">Imagen:</label>
                                        <div class="col-lg-9">
                                            <input type="file"  id="imagen" class="form-control" placeholder="Archivo de imagen .jpg .png">

                                        </div>
                                        <img id="img" src="" height="100">
                                    </div>
                                    <div id="stategroup" class="form-group">
                                        <label class="col-lg-3 control-label"><i class="glyphicon glyphicon-star position-left" ></i>Estado Actual:</label>
                                        <div class="col-lg-9">
                                            <select id="status" class="form-control"  style="width: 100%;" >
                                                <option value="0">Inactivado</option>
                                                <option value="1">Activo</option>

                                            </select>
                                        </div>
                                    </div>

                                </fieldset>

                            </div>
                        </div>
                    </div>

                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-info" id="guardar_cambios">Guardar Cambios</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ URL::asset('/javascript/motos.js') }}" type="text/javascript"></script>
    <script type="application/javascript">
        $('#btnbusqueda').click(function(){
           var query = $('#busqueda_nombre').val();
           var link = "?query="+query;
           window.location = "{{ url('/list_motos')}}"+link;
        });
    </script>
@stop