var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
var edit  = 0;

$('#categorias').on('click','#editar',function(){
	console.log("click");
    $('#formulario').modal('show');

});

// Basic setup
$(".colorpicker-basic").spectrum();


/**********BOTONES DENTRO DE LA TABLA PRODUCTOS ****************/

$('#categorias_table').on('click', '#detalles', function(event) {
    limpiar();
    event.preventDefault();
    id = $(this).data('product');
    window.location.replace(currentLocation+'single_product?id='+id);
});

$('#categorias_table').on('click','#eliminar' ,function() {
    limpiar();
    idcategoria=  $(this).data('idcategoria');

    swal({
            title: "Estas seguro?",
            text: "No podras recuperar este producto si lo eliminas!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, salir",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                var jqxhr =  $.post(currentLocation+"category_delete",{id:idcategoria},function(data,status){

                }).done(function() {
                    swal({
                        title: "Eliminado!",
                        text: "La categoria ha sido eliminado.",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                    },function(){
                        window.location.reload();
                    });
                }).fail(function() {
                    swal("Error al eliminar producto", "Intentelo nuevamente luego.", "error");
                });
            }
            else {
                swal({
                    title: "Cancelado",
                    text: "No se ha eliminado nada :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
});

$('#products_table').on('click','#editar',function(){
    limpiar();
    edit = 1;
    idcategoria=  $(this).data('idcategoria');
    descripcion = $(this).data('descripcion');
    padre = $(this).data('padre');
    Pcolor = $(this).data('color');


    $('#idcategoria').val(idcategoria);
    $('#descripcion').val(descripcion);
    $('#padre').val(padre).change();
    $('#color').spectrum({
        color: Pcolor
    });




    $('#formulario').modal('show');
});
/***************************************************************/

/***********ACCIONES DENTRO DEL MODAL EDITAR Y CREAR ***********/
$('#formulario').on('click','#guardar_cambios',function(){
    idcategoria = $('#idcategoria').val();
    descripcion = $('#descripcion').val();
    padre = $('#padre').val();
    color  = $('#color').spectrum("get").toHexString();




    if(validar_datos()){
        arrayPost = {idcategoria:idcategoria, descripcion:descripcion, idpadre:padre, color:color, edit:edit};
        console.log(arrayPost);
        var jqxhr =  $.post(currentLocation+"category_store",arrayPost,function(data,status){

      }).done(function() {
          $('#formulario').modal('hide');

          swal({
                  title: "Bien hecho!",
                  text: "Se guardo correctamente",
                  type: "success"
              },
              function(){
                  window.location.reload()
              });
          ;
      }).fail(function() {
          swal("Error al guardar", "Intentelo nuevamente luego.", "error");
      });

            $('#formulario').modal('hide');
        swal("Bien hecho!", "Se edito correctamente", "success")
    }

});

/***************************************************************/

/*****************NUEVO PRODUCTO********************************/
$('#nuevo_producto').on('click',function(){
    limpiar();
    edit = 0;
    $('#idcategoria').val('');
    $('#descripcion').val('');
    $('#padre').val(0).change();

    $('#formulario').modal('show');
    if(validar_datos()){
        var array_datos = {}
    }

});

/****************************************************************/
/*****************VALIDAR****************************************/

function validar_datos(){
    descripcion =  $('#descripcion').val();


    if(descripcion  === undefined || descripcion === ''){
        $('#nombregroup').addClass("has-error");
        return false;
    }


    return true;

}

function limpiar(){
    $('#nombregroup').removeClass("has-error");
    $('#categoriagroup').removeClass("has-error");
    $('#color').spectrum({
        color: "#20BF7E"
    });
}

/****************************************************************/

/*
$('#guardar').click(function(event) {
  categID = $('#categID').val();
  description = $('#description').val();
  padre = null;


  if($('#padre').val() !== 0){
  	padre = $('#padre').val();
  }

  if(padre == categID  ){s
  	toastr["info"]("Escoger otra categoria padre.");
  	return ;
  }

  if(categID !== ''){
  	if(description !== ''){
  			$.post(currentLocation+'category_update', {description: description, id: categID , parent:padre}, function(data, textStatus, xhr) {
		        window.location.replace(currentLocation+'list_categorys');
		        $('#categID').val('');
		      }).error(function(jqXHR, textStatus, errorThrown) {
		      	console.log(jqXHR);
		      	  if (jqXHR.status === 409) {
				   toastr["warning"]("Conflicto de Categorias!, prueba ingresando una categoria padre que no sea hijo de la primera.");
				  }else{
				  	 toastr["error"]("No se logro guardar la categoria, intentelo mas tarde");
				  }
		       
		      });
  	}else{
  		toastr["warning"]("Debes escribir la descripcion de la categoria.");
  		return;
  	}
  
  }else{
  	if(description !== ''){
	  		$.post(currentLocation+'category_store', {description: description, id: categID , parent:padre}, function(data, textStatus, xhr) {
	        window.location.replace(currentLocation+'list_categorys');
	        $('#categID').val('');
	      }).error(function() {
	        toastr["error"]("No se logro guardar el producto, intentelo mas tarde")
	      });
	  }else{
	  	toastr["warning"]("Debes escribir la descripcion de la categoria.");
  		return;
	  }
  }

  
});

$('#eliminar').click(function(event) {
	 categID = $('#categID').val();
	  if(categID !== ''){
			$.post(currentLocation+'category_delete', {id: categID}, function(data, textStatus, xhr) {
		        window.location.replace(currentLocation+'list_categorys');
		        $('#categID').val('');
		      }).error(function() {
		        toastr["error"]("No se logro guardar el eliminar, Puede que la categoria este siendo usada.")
		      });
	  }else{
	  		 toastr["warning"]("Escoger categoria.")
	  }

});
*/
