var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';

$('#moto_table').on('click','#eliminar' ,function() {
    limpiar();
    idmotos=  $(this).data('idmotos');

    swal({
            title: "Estas seguro?",
            text: "No podras recuperar este producto si lo eliminas!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, salir",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                var jqxhr =  $.post(currentLocation+"moto_delete",{idmotos:idmotos},function(data,status){

                }).done(function() {
                    swal({
                        title: "Eliminado!",
                        text: "La Marca ha sido eliminado.",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                    },function(){
                        window.location.reload();
                    });
                }).fail(function() {
                    swal("Error al eliminar marca", "Intentelo nuevamente luego.", "error");
                });
            }
            else {
                swal({
                    title: "Cancelado",
                    text: "No se ha eliminado nada :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
});


