var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';

$('#btn_guardar').click(function(event){
    var idcliente = $('#idcliente').val();
    var dni = $('#dni').val();
    var ruc = $('#ruc').val();
    var nombres = $('#nombres').val();
    var apellidos = $('#apellidos').val();
    var direccion = $('#direccion').val();
    var email = $('#email').val();
    var telefono = $('#telefono').val();

    console.log(idcliente);

    if(dni.length == 0){
        swal({
            title: "Upss!",
            text: "Debes agregar el DNI del cliente",
            confirmButtonColor: "#66BB6A",
            type: "error"
        },function(){
            //window.location.reload();
        });
        return;
    }
    if(nombres.length == 0){
        swal({
            title: "Upss!",
            text: "Debes agregar los nombres del Cliente",
            confirmButtonColor: "#66BB6A",
            type: "error"
        },function(){
            //window.location.reload();
        });
        return;
    }
    if(apellidos.length == 0){
        swal({
            title: "Upss!",
            text: "Debes agregar los apellidos del cliente",
            confirmButtonColor: "#66BB6A",
            type: "error"
        },function(){
            //window.location.reload();
        });
        return;
    }

    console.log(idcliente);
    $.post(currentLocation+'guardar_cliente',{icliente:idcliente, dni:dni, ruc:ruc, nombres:nombres, apellidos:apellidos,direccion:direccion,telefono:telefono,email:email},function(data){
        obj = JSON.parse(data);
        if(obj.mensaje === 200){
            swal({
                title: "Ok!",
                text: "Se guardo correctamente!.",
                confirmButtonColor: "#66BB6A",
                type: "success"
            },function(){
               // window.location.reload();
            });
            return;
        }else{
            swal({
                title: "Error..!",
                text: "No se puede guardar el cliente, intentalo de nuevo luego.",
                confirmButtonColor: "#66BB6A",
                type: "error"
            },function(){
                window.location.reload();
            });
            return;
        }
    });

});
