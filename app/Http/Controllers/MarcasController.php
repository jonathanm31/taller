<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use JWTAuth;

use Dingo\Api\Routing\Helpers;
use App\Models\Marcas;
use DB;
use App\User;
use Auth;

class MarcasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    }
    public function create(Request $request)
    {
        $empresa = Auth::user()->idempresa;

        $cant = $request['cant'];
        $query = $request['query'];

        if(!isset($cant))$cant = 25;
        if(!isset($query)){
            $marcas = DB::table('marcas')
                ->where('idempresa',$empresa)
                ->limit(1000)
                ->paginate($cant);
        }else{
            $marcas = DB::table('marcas')
                ->where('idempresa',$empresa)
                ->where('nombre','like','%'.$query.'%')
                ->paginate($cant);

        }

        return view('marcas/marcas', ['marcas' => $marcas]);
    }

    public function store(Request $request){

        $user = Auth::user()->idempresa;
        $idmarca= $request['idmarca'];
        $descripcion = $request['descripcion'];
        $nombre = $request['nombre'];
        $logo = $request->file('imagen');
        $editar = $request['edit'];
        date_default_timezone_set("UTC");

        if($editar){
            $marca = Marcas::find($idmarca);
            $marca->descripcion = $descripcion;
            $marca->nombre = $nombre;
            $marca->idempresa = $user;
            $marca->state = 1;

            if(!empty($logo)){
                $imageName = 'marca'.time().'.jpg';
                $marca->img = $imageName;
                Storage::put('marcas/'.$imageName,file_get_contents($logo));

            }
            $marca->save();
        }else{
            $marca = new Marcas;
            $marca->descripcion = $descripcion;
            $marca->nombre = $nombre;
            $marca->idempresa = $user;
            $marca->state = 1;


            $imageName = 'marca'.time().'.jpg';

            if(!empty($logo)){
                $imageName = 'marca'.time().'.jpg';
                $marca->img = $imageName;
                Storage::put('marcas/'.$imageName,file_get_contents($logo));

            }

            $marca->save();
        }

        return response()->json(array('success' => true));
    }
    public function delete(Request $request){
        $id = $request['idmarca'];
        $marca = Marcas::find($id);

        try {
            if($marca->img){
                Storage::delete('marcas/'.$marca->img);

            }
            $marca->delete();
            return response()->json(['accepted'], 202);
        } catch (Exception $e) {
            return response()->json(['conflict'], 409);
        }
    }
    public function nuevaMarca(){
        return view('marcas.nueva_marca');
    }
    public function editarMarca(Request $request){
        $marca = Marcas::find($request['idmarca']);
        return view('marcas.editar_marca')->with(['marca' => $marca]);
    }
}