<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JWTAuth;

use App\Models\Transacciones;
use Dingo\Api\Routing\Helpers;
use App\Models\Categoria;
use App\Models\Producto;
use App\User;
use App\Models\CajaH;
use App\Models\CajaD;
use Auth;
use DB;


class InventarioController extends Controller
{
    public function __construct()
    {
    }
    public function entradas_index()
    {
       $id = Auth::user()->id;

        $usuario = DB::table('usuarios')
            ->where('id',$id)
            ->join('sucursales','usuarios.idsucursal','=','sucursales.idsucursal')
            ->join('empresas','usuarios.idempresa','=','empresas.idempresa')
            ->first();


        return view('inventario/entradas')->with('usuario',$usuario);
    }
    public function salidas_index()
    {
       $id = Auth::user()->id;

        $usuario = DB::table('usuarios')
            ->where('id',$id)
            ->join('sucursales','usuarios.idsucursal','=','sucursales.idsucursal')
            ->join('empresas','usuarios.idempresa','=','empresas.idempresa')
            ->first();


        return view('inventario/salidas')->with('usuario',$usuario);
    }

    public function buscar_inventario(Request $request){
        $busqueda = $request['query'];
        $products = DB::table('producto')
            ->select('producto.*','categorias.color as color','categorias.descripcion as categoria')
            ->where('producto.tipo','=',1)
            ->Where(function ($query) {
                $sucursal = Auth::user()->idsucursal;
                $query->where('producto.cantidad','>=',1)
                    ->orwhere('producto.state',1)
                    ->orwhere('idsucursal',$sucursal);
            })
            ->where('producto.barcode', 'like', '%'.$busqueda.'%')
            ->orwhere('producto.nombre', 'like', '%'.$busqueda.'%')
            ->orwhere('categorias.descripcion', 'like', '%'.$busqueda.'%')

            ->leftJoin('categorias', 'producto.idcategoria', '=', 'categorias.idcategoria')
            ->get();

        return json_encode($products);
    }

    public function store_entradas(Request $request){
        $empresa = Auth::user()->idempresa;
        $sucursal = Auth::user()->idsucursal;
        $productos = json_decode($request['productos']);


        foreach ($productos as $producto){
            if($producto->cantidad > 0){
                $transacts = new Transacciones;
                $transacts->idproducto = $producto->idproducto;
                $transacts->idempresa = $empresa;
                $transacts->idsucursal = $sucursal;

                $transacts->cantidad = $producto->cantidad;
                $transacts->state = 1;
                $transacts->tipo = 2; // 1 entrada , 0 salidas
                $transacts->save();

                $prod = Producto::find($producto->idproducto);
                $prod->cantidad = $prod->cantidad + $producto->cantidad;
                if($prod->cantidad > 0){
                    $prod->state = 1;
                }
                $prod->save();
            }
        }
        return response()->json(['created'], 200);
    }

    public function store_salidas(Request $request){
        $empresa = Auth::user()->idempresa;
        $sucursal = Auth::user()->idsucursal;
        $productos = json_decode($request['productos']);


        foreach ($productos as $producto){
            if($producto->cantidad > 0){
                $transacts = new Transacciones;
                $transacts->idproducto = $producto->idproducto;
                $transacts->idempresa = $empresa;
                $transacts->idsucursal = $sucursal;

                $transacts->cantidad = $producto->cantidad;
                $transacts->state = 1;
                $transacts->tipo = 2; // 1 entrada , 0 salidas
                $transacts->save();

                $prod = Producto::find($producto->idproducto);
                $prod->cantidad = $prod->cantidad - $producto->cantidad;
                if($prod->cantidad < 0){
                    $prod->state = 0;
                }
                $prod->save();
            }
        }
        return response()->json(['created'], 200);
    }

}