<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use JWTAuth;

use Dingo\Api\Routing\Helpers;

use App\Models\Cliente;
use DB;
use App\User;
use Auth;


class ClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $empresa = Auth::user()->idempresa;

        $cant = $request['cant'];
        $query = $request['query'];

        if(!isset($cant))$cant = 20;
        if(!isset($query)){
            $clientes = DB::table('clientes')
                ->where('idempresa',$empresa)
                ->limit(1000)
                ->paginate($cant);
        }else{
            $clientes = DB::table('clientes')
                ->where('idempresa',$empresa)
                ->where('nombre','like','%'.$query.'%')
                ->paginate($cant);

        }

        return view('cliente/clientes', ['clientes' => $clientes]);
    }
    public function create(Request $request)
    {
        return view('cliente/nuevoCliente');
    }

    public function update(Request $request){
        $idcliente = $request['idcliente'];
        $cliente = Cliente::find($idcliente);

        return view('cliente/editarCliente')->with('cliente',$cliente);
    }

    public function store(Request $request){
        $empresa = Auth::user()->idempresa;
        $idcliente = $request['idcliente'];
        $dni = $request['dni'];
        $ruc = $request['ruc'];
        $nombres = $request['nombres'];
        $apellidos = $request['apellidos'];
        $telefono = $request['telefono'];
        $email = $request['email'];
        $direccion = $request['direccion'];



        if(!empty($idcliente)){
            $cliente = Cliente::find($idcliente);
            $cliente->idempresa = $empresa;
            $cliente->dni = $dni;
            $cliente->ruc = $ruc;
            $cliente->nombres = $nombres;
            $cliente->apellidos = $apellidos;
            $cliente->telefono = $telefono;
            $cliente->email = $email;
            $cliente->direccion = $direccion;

            $cliente->save();
        }else{
            $cliente = new Cliente;
            $cliente->idempresa = $empresa;
            $cliente->dni = $dni;
            $cliente->ruc = $ruc;
            $cliente->nombres = $nombres;
            $cliente->apellidos = $apellidos;
            $cliente->telefono = $telefono;
            $cliente->email = $email;
            $cliente->direccion = $direccion;

            $cliente->save();
        }
        return json_encode(['mensaje' => 200, 'idcliente' => $cliente->idcliente]);
    }
    public function delete(Request $request){
        $idcliente = $request['idcliente'];
        $cliente = Cliente::find($idcliente);
        $cliente->delete();
        return json_encode(['mensaje' => 200]);
    }
}