<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
use Imagick;
use Dingo\Api\Routing\Helpers;
use App\Models\Transacciones;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Motos;

use DB;
use App\User;
use Auth;
use Psy\Util\Json;
use Image;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $user = Auth::user()->idsucursal;
        $empresa = Auth::user()->idempresa;

        $cant = $request['cant'];
        $query = $request['query'];


        if(!isset($cant))$cant = 20;
        if(!isset($query)){
            $products = DB::table('producto')
                ->select('categorias.descripcion as categoria','producto.*')
                ->where('producto.tipo','=',1)
                ->where('idsucursal',$user)
                ->join('categorias', 'producto.idcategoria', '=', 'categorias.idcategoria')
                ->orderBy('producto.nombre', 'asc')
                ->limit(1000)
                ->paginate(20);

        }else{
            $products = DB::table('producto')
                ->select('categorias.descripcion as categoria' ,'producto.*')
                ->where('producto.tipo','=',1)
                ->where('idsucursal',$user)
                ->where( function ($q) use ($query) {
                     $q->where('producto.nombre','like','%'.$query.'%')
                         ->orwhere('producto.barcode','like','%'.$query.'%');
                 })
                ->join('categorias', 'producto.idcategoria', '=', 'categorias.idcategoria')
                ->orderBy('producto.nombre', 'asc')
                ->paginate(100);
        }
        $categorias = DB::table('categorias')->where('idempresa',$empresa )
           ->get();


        $motos = DB::table('motos')->get();
        $marcas = DB::table('marcas')->where('idempresa',$empresa)->get();

        return view('products/lista_producto', ['products' => $products])->with('categorias', $categorias)->with('motos',$motos)->with('marcas',$marcas);

    }

    public function create(Request $request)
    {
        $empresa = Auth::user()->idempresa;
        $categorias = DB::table('categorias')->where('idempresa',$empresa )
            ->get();


        $motos = DB::table('motos')->get();
        $marcas = DB::table('marcas')->where('idempresa',$empresa)->get();
        return view('products/nuevo_producto')->with('categorias', $categorias)->with('motos',$motos)->with('marcas',$marcas);

    }

    public function update(Request $request){
        $query = $request['id'];
        $user = Auth::user()->idsucursal;
        $empresa = Auth::user()->idempresa;
        if(!empty($query)){
            $categorias = DB::table('categorias')->where('idempresa',$empresa )
                ->get();


            $motos = DB::table('motos')->get();
            $marcas = DB::table('marcas')->where('idempresa',$empresa)->get();
            $producto = DB::table('producto')
                ->where('idsucursal',$user)
                ->where('idempresa',$empresa)
                ->where('idproducto',$query)
                ->where('tipo',1)
                ->first();

            $prodmotos = DB::table('motos')
                        ->select('motos.idmoto')
                        ->join('productomotos','motos.idmoto','=','productomotos.idmoto')
                        ->where('productomotos.idproducto',$query)
                        ->get();

            if( !empty($producto) or !isset($producto)){
                return view('products/editar_producto',['mensaje' => '200'])->with('producto', $producto)->with('categorias', $categorias)->with('motos',$motos)->with('marcas',$marcas)->with('motosProducto',$prodmotos);
             }else{
                return json_encode(['mensaje' => '404']);
            }
        }
        else{
            return json_encode(['mensaje' => '404']);
        }
    }

    public function find(Request $request){
        $query = $request['nom'];
        $user = Auth::user()->idsucursal;
        $empresa = Auth::user()->idempresa;

        if(!empty($query)){
            $products = DB::table('producto')
                ->where('idsucursal',$user)
                ->where('idempresa',$empresa)
                ->where('producto.nombre','like','%'.$query.'%')
                ->get();

            return response()->json($products, 202);
        }



    }

    public function get(Request $request){
        $query = $request['id'];
        $user = Auth::user()->idsucursal;
        $empresa = Auth::user()->idempresa;

        if(!empty($query)){
            $products = DB::table('producto')
                ->where('idsucursal',$user)
                ->where('idempresa',$empresa)
                ->where('idproducto',$query)
                ->where('tipo',1)
                ->first();
            //TIPO 1 PRODUCTO, 2 SERVICIO
            return response()->json($products, 202);
        }
    }

    public function status(Request $request){
        $id = $request['id'];
        $status = $request['status'];
        $producto = Producto::find($id);
        $producto->state = $status;
            $producto->save();
            return response()->json(['accepted'], 202);
    }

    public function store(Request $request){
        $sucursal = Auth::user()->idsucursal;
        $empresa = Auth::user()->idempresa;
        $tipo = $request['tipo'];
        $idproducto = $request['idproducto'];
        $nombre = $request['nombre'];
        $idcategoria = $request['categoria'];
        $idmarca = $request['idmarca'];
        $costo = $request['costo'];
        $cantidad = $request['cantidad'];
        $flete = $request['flete'];
        $stock_min = $request['stock_min'];
        $barcode = $request['barcode'];
        $precio = $request['precio'];
        $estado = $request['state'];
        $descripcion = $request['descripcion'];
        $procedencia = $request['procedencia'];
        $fabricante = $request['fabricante'];
        $proveedor = $request['proveedor'];
        $ubicacion = $request['ubicacion'];
        $motos = $request['motos'];
        $ids = $request['ids'];
        $imagenes = $request->file('imagenes');
        $motos = json_decode($motos);
        $ids = explode(",", $ids);
        if(!empty($idproducto)){


           $product = Producto::find($idproducto);
           if($nombre){
               $product->nombre = $nombre;
               dump($nombre);
           }
            if($idcategoria){
                $product->idcategoria = $idcategoria;
                dump($idcategoria);
            }
            if($idmarca){
                $product->idmarca = $idmarca;

            }
            if($cantidad){
                $product->cantidad = $cantidad;
            }
            if($flete){
                $product->flete = $flete;
            }
            if($stock_min){
                $product->stock_min = $stock_min;
            }
            if($barcode){
                $product->barcode = $barcode;
            }
            if($ubicacion){
                $product->ubicacion = $ubicacion;
            }
            if($precio){
                $product->precio = $precio;
            }
            if($costo){
                $product->costo = $costo;
            }

            if($estado){
                $product->state = $estado;
            }
            if($descripcion){
                $product->descripcion = $descripcion;
            }
            if($procedencia){
                $product->procedencia = $procedencia;
            }
            if($fabricante){
                $product->fabricante = $fabricante;
            }
            if($proveedor){
                $product->proveedor = $proveedor;
            }


            $product->tipo = $tipo;


            $imagenes_prod = json_decode($product->imagenes);

           if( isset($imagenes)){
               for($k = 0; $k < count($imagenes); $k++){
                   /*****BORRAMOS IMAGENES CHANCADAS EN EL ARRAY PREVIO*****/

                   Storage::delete('productos/'.$imagenes_prod->array[intval($ids[$k])]);
                   Storage::delete('productos/thumbs_180_140/'.$imagenes_prod->array[intval($ids[$k])]);
                   Storage::delete('productos/thumbs_700_510/'.$imagenes_prod->array[intval($ids[$k])]);

                   /*****CREAMOS LAS NUEVAS IMAGENES************************/
                   $name = "imagen".$k.time().'.jpg';

                   Storage::put('productos/'.$name,file_get_contents($imagenes[$k]));
                   $img1 = Image::make(Storage::get('productos/'.$name))->resize(180, 140);
                   $img2 = Image::make(Storage::get('productos/'.$name))->resize(700, 510);

                   $img1->save('productos/thumbs_180_140/'.$name);
                   $img2->save('productos/thumbs_700_510/'.$name);

                   /****ACTUALIZAMOS ARRAY DE IMAGENES********************/
                   $imagenes_prod->array[intval($ids[$k])] = $name;

               }
               $product->imagenes = json_encode($imagenes_prod);
           }
           $product->save();

            if($motos && count($motos) > 0){
                $product->motos()->sync($motos);
            }else{
                $product->motos()->detach();
            }


         }else{
            $i = 0;
            $product = new Producto;
            $files_img = [];
            if(count($imagenes) > 0){
                for($j = 0; $j < count($imagenes); $j++){
                    $name = "imagen".$j.time().'.jpg';
                    $imagen = $imagenes[$j];
                    Storage::put('productos/'.$name,file_get_contents($imagen));
                    $img1 = Image::make(Storage::get('productos/'.$name))->resize(180, 140);
                    $img2 = Image::make(Storage::get('productos/'.$name))->resize(700, 510);
                    $img1->save('productos/thumbs_180_140/'.$name);
                    $img2->save('productos/thumbs_700_510/'.$name);

                    $files_img[]= $name;
                    $i++;
                }
                for(;$i<4;$i++){
                    $files_img[]=  "null";
                }
                $product->imagenes = json_encode(['array'=>$files_img]);
            }else{
                if($tipo == 1){
                    $arrImg[] = "productos_m_c.png";
                }else{
                    $arrImg[] = "servicios.png";
                }
                $arrImg[] = "null";
                $arrImg[] = "null";
                $arrImg[] = "null";

                $product->imagenes = json_encode(['array'=> $arrImg ]);
            }


             $product->idempresa = $empresa;
             $product->idsucursal = $sucursal;
             $product->nombre = $nombre;
             $product->precio = $precio;
             $product->tipo = $tipo;

                if($idcategoria){
                    $product->idcategoria = $idcategoria;

                }
                if($idmarca){
                    $product->idmarca = $idmarca;

                }
                if($cantidad){
                    $product->cantidad = $cantidad;
                }
                if($barcode){
                    $product->barcode = $barcode;
                }
                if($ubicacion){
                    $product->ubicacion = $ubicacion;
                }
                if($flete){
                    $product->flete = $flete;
                }
                if($stock_min){
                    $product->stock_min = $stock_min;
                }

                if($costo){
                    $product->costo = $costo;
                }
                if($estado){
                    $product->state = $estado;
                }
                if($descripcion){
                    $product->descripcion = $descripcion;
                }
                if($procedencia){
                    $product->procedencia = $procedencia;
                }
                if($fabricante){
                    $product->fabricante = $fabricante;
                }
                if($proveedor){
                    $product->proveedor = $proveedor;
                }


             $product->save();

            if($motos && count($motos) > 0){
                $product->motos()->sync($motos);
            }else{
                $product->motos()->detach();
            }
         }

         if($cantidad > 0){

             $transacts = new Transacciones;
             $transacts->idproducto = $product->idproducto;
             $transacts->idempresa = $empresa;
             $transacts->idsucursal = $sucursal;

             $transacts->cantidad = $cantidad;
             $transacts->state = 1;
             $transacts->tipo = 1;

             $transacts->save();
         }

         return response()->json(['created'], 201);

         }

     public function destroy(Request $request){
         /*$currentUser = JWTAuth::parseToken()->authenticate();
         $token = JWTAuth::getToken();
         $user = JWTAuth::toUser($token);*/

        $id = $request['id'];
        $product = Producto::find($id);
        try {
        	$product->delete();
        	return response()->json(['accepted'], 202);
        } catch (Exception $e) {
        	return response()->json(['conflict'], 409);
        }

        
    }

    public function productoInfo(Request $request){
        $query = $request['id'];
        $user = Auth::user()->idsucursal;
        $empresa = Auth::user()->idempresa;
        if(!empty($query)){
            $categorias = DB::table('categorias')->where('idempresa',$empresa )
                ->whereNotNull('idpadre')
                ->where('idpadre','>',0)
                ->get();


            $motos = DB::table('motos')->get();
            $marcas = DB::table('marcas')->where('idempresa',$empresa)->get();
            $producto = DB::table('producto')
                ->where('idsucursal',$user)
                ->where('idempresa',$empresa)
                ->where('idproducto',$query)
                ->where('tipo',1)
                ->first();

            $prodmotos = DB::table('motos')
                ->select('motos.idmoto')
                ->join('productomotos','motos.idmoto','=','productomotos.idmoto')
                ->where('productomotos.idproducto',$query)
                ->get();

            if( !empty($producto) or !isset($producto)){
                return view('products/info_producto',['mensaje' => '200'])->with('producto', $producto)->with('categorias', $categorias)->with('motos',$motos)->with('marcas',$marcas)->with('motosProducto',$prodmotos);
            }else{
                return json_encode(['mensaje' => '404']);
            }
        }
        else{
            return json_encode(['mensaje' => '404']);
        }

    }

    public function get_motos(Request $request){

        $idproducto = $request['id'];
        $motos = DB::table('productomotos')
                    ->select('motos.idmoto')
                    ->where('productomotos.idproducto',$idproducto)
                    ->join('motos','productomotos.idmoto','=','motos.idmoto')
                    ->get();
        $idsmotos = array();

        foreach ($motos as $moto){
            array_push($idsmotos,$moto->idmoto);
        }

        return response($idsmotos);
    }


   
}