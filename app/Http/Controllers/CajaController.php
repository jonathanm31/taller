<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JWTAuth;

use App\Models\Transacciones;
use Dingo\Api\Routing\Helpers;
use App\Models\Categoria;
use App\Models\Producto;
use App\User;
use App\Models\CajaH;
use App\Models\CajaD;
use Auth;
use DB;


class CajaController extends Controller
{
    public function __construct()
    {
    }
    public function index()
    {
        $empresa = Auth::user()->idempresa;
        $sucursal = Auth::user()->idsucursal;
        $id = Auth::user()->id;

        $products = Producto::where('idempresa',$empresa)
                    ->where('idsucursal',$sucursal)
                    ->where('state',1)
                    ->where('cantidad','>',0)->get();

        $usuario = DB::table('usuarios')
                        ->where('id',$id)
                        ->join('sucursales','usuarios.idsucursal','=','sucursales.idsucursal')
                        ->join('empresas','usuarios.idempresa','=','empresas.idempresa')
                        ->first();


        return view('caja/caja')->with('products',$products)->with('usuario',$usuario);
    }

    public function buscar_producto(Request $request){
        $busqueda = $request['query'];
        $products = DB::table('producto')
            ->select('producto.*','categorias.color as color','categorias.descripcion as categoria')
            ->Where(function ($query) {
                $sucursal = Auth::user()->idsucursal;
                $query->where('producto.cantidad','>=',1)
                    ->orwhere('producto.state',1)
                    ->orwhere('idsucursal',$sucursal);
            })
             ->where('producto.barcode', 'like', '%'.$busqueda.'%')
             ->orwhere('producto.nombre', 'like', '%'.$busqueda.'%')
             ->orwhere('categorias.descripcion', 'like', '%'.$busqueda.'%')

            ->leftJoin('categorias', 'producto.idcategoria', '=', 'categorias.idcategoria')
            ->get();

        return json_encode($products);
    }

    public function buscar_cliente(Request $request){
        $busqueda = $request['query'];
        $clientes = DB::table('clientes')
            ->where('nombres','like','%'.$busqueda.'%')
            ->orwhere('apellidos','like','%'.$busqueda.'%')
            ->orwhere('dni',$busqueda)
            ->orwhere('ruc',$busqueda)
            ->get();
        return json_encode($clientes);
    }


    public function store(Request $request){

        $empresa = Auth::user()->idempresa;
        $sucursal = Auth::user()->idsucursal;
        $idusuario = Auth::user()->id;

        $idcliente = $request['idcliente'];
        $igv = $request['igv'];
        $total = $request['total'];
        $paga = $request['paga'];
        $vuelto = $request['vuelto'];
        $descuento = $request['descuento'];
        $comentarios = $request['comentarios'];

        $productos_json = $request['productos'];
        $tipo_recibo = $request['tipo']; // 1 boleta , 2 factura;
        $state = 1;


        $cajah = new CajaH;
        $cajah->idempresa = $empresa;
        $cajah->idsucursal = $sucursal;
        $cajah->idusuario = $idusuario;
        $cajah->idcliente = $idcliente;

        $maximo_num = DB::table('cajah')
                        ->where('tipo', $tipo_recibo)
                        ->where('idempresa',$empresa)
                        ->where('idsucursal',$sucursal)
                        ->max('numeracion');

        $cajah->numeracion = intval($maximo_num) + 1 ;

        $cajah->paga = $paga;
        $cajah->igv = $igv;
        $cajah->vuelto = $vuelto;
        $cajah->descuento = $descuento;
        $cajah->total = $total;
        $cajah->tipo = $tipo_recibo;
        $cajah->comentarios = $comentarios;
        $cajah->save();


        $productos = json_decode($productos_json);

        for($i = 0; $i < count($productos); $i++){
            $product = Producto::find($productos[$i]->idproducto);
            if($product->tipo == 1){
                        $product->cantidad = $product->cantidad - $productos[$i]->cantidad;
                        $product->save();
            }
            $cajad = new CajaD;
            $cajad->idcajah = $cajah->idcajah;
            $cajad->idproducto = $productos[$i]->idproducto;
            $cajad->cantidad = $productos[$i]->cantidad;
            $cajad->precio = $productos[$i]->precio;
            $cajad->idempresa = $empresa;
            $cajad->save();

            $transacts = new Transacciones;
            $transacts->idproducto = $productos[$i]->idproducto;
            $transacts->cantidad = $productos[$i]->cantidad;
            $transacts->tipo = 0;
            $transacts->state = 1;
            $transacts->idsucursal = $sucursal;
            $transacts->idempresa = $empresa;
            $transacts->idempresa = $empresa;
            $transacts->idcajah = $cajah->idcajah;
            $transacts->save();

        }

        $respuesta = array();
        $respuesta[]= ['created'=> 200];
        $respuesta[] = ['id' => $cajah->idcajah];

        return json_encode($respuesta);

    }
    public function show(Request $request){
    	$id = $request['id'];

    	$caja = DB::table('cajah')
                ->where('idcajah',$id)
                ->first();

    	$cajaD = DB::table('cajad')
                ->select('cajad.*','producto.nombre')
                ->where('cajad.idempresa','=',$caja->idempresa)
                ->where('cajad.idcajah','=',$caja->idcajah)
                ->join('producto','cajad.idproducto','=','producto.idproducto')
                ->get();

        $sucursal = DB::table('sucursales')
                    ->where('sucursales.idsucursal','=',$caja->idsucursal)
                    ->join('empresas','sucursales.idempresa','=','empresas.idempresa')
                    ->first();

        $cliente = DB::table('clientes')
                    ->where('idcliente','=',$caja->idcliente)
                    ->first();

        return view('caja/info_caja')
            ->with('caja',$caja)
            ->with('cajaD',$cajaD)
            ->with('sucursal',$sucursal)
            ->with('cliente',$cliente);

    }


}