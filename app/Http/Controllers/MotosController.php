<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use JWTAuth;

use Dingo\Api\Routing\Helpers;
use App\Models\Motos;
use App\Models\Marcas;
use DB;
use App\User;
use Auth;


class MotosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    }
    public function create(Request $request)
    {

        $empresa = Auth::user()->idempresa;

        $cant = $request['cant'];
        $query = $request['query'];

        if(!isset($cant))$cant = 25;
        if(!isset($query)){

            $motos = DB::table('motos')
                ->select('motos.nombre','motos.state','motos.descripcion','marcas.nombre as marca','motos.imagen','motos.serie','motos.idmoto','marcas.idmarca')
                ->join('marcas','motos.idmarca', '=','marcas.idmarca')
                ->limit(1000)
                ->paginate($cant);
        }else{
            $motos = DB::table('motos')
                ->select('motos.nombre','motos.state','motos.descripcion','marcas.nombre as marca','motos.imagen','motos.serie','motos.idmoto','marcas.idmarca')
                ->join('marcas','motos.idmarca', '=','marcas.idmarca')
                ->where('motos.nombre','like','%'.$query.'%')
                ->paginate($cant);

        }
        $marcas = DB::table('marcas')->where('idempresa',$empresa)->get();

        return view('motos/motos', ['motos' => $motos],['marcas' => $marcas]);
    }

    public function store(Request $request){
        $empresa = Auth::user()->idempresa;
        $idmoto = $request['idmoto'];
        $idmarca = $request['idmarca'];
        $nombre = $request['nombre'];
        $descripcion = $request['descripcion'];
        $imagen = $request->file('imagen');
        $serie = $request['serie'];
        $state = $request['status'];
        $edit = $request['edit'];

        if($edit){
            $moto = Motos::find($idmoto);
            $moto->idmarca = $idmarca;
            $moto->nombre = $nombre;
            $moto->descripcion = $descripcion;
            $moto->state = $state;
            $moto->serie = $serie;
            if(!empty($imagen)){
                $imageName = 'marca'.time().'.jpg';
                $moto->imagen = $imageName;
                Storage::put('motos/'.$imageName,file_get_contents($imagen));

            }


            $moto->save();

        }else{
            $moto = new Motos;
            $moto->idmarca = $idmarca;
            $moto->nombre = $nombre;
            $moto->descripcion = $descripcion;
            $moto->state = $state;
            $moto->serie = $serie;
            if(!empty($imagen)){
                $imageName = 'marca'.time().'.jpg';
                $moto->imagen = $imageName;
                Storage::put('motos/'.$imageName,file_get_contents($imagen));

            }
            $moto->save();
        }

    }
    public function delete(Request $request){
        $id = $request['idmoto'];
        $moto = Motos::find($id);

        try {
            Storage::delete('motos/'.$moto->img);
            $moto->delete();
            return response()->json(['accepted'], 202);
        } catch (Exception $e) {
            return response()->json(['conflict'], 409);
        }
    }

    public function nuevaMoto(){
        $marcas = Marcas::all();
        return view('motos.nueva_moto')->with('marcas',$marcas);
    }
    public function editarMoto(Request $request){
        $moto = Motos::find($request['idmoto']);
        $marcas = Marcas::all();
        return view('motos.editar_moto')->with(['moto' => $moto, 'marcas' => $marcas]);
    }
}