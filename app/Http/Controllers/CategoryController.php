<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JWTAuth;

use Dingo\Api\Routing\Helpers;
use App\Models\Categoria;
use App\User;
use Auth;
use DB;

class CategoryController extends Controller
{
    public function __construct()
    {
    }
    public function index()
    {
        /*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $user = Auth::user()->idempresa;
        $categorys = DB::table('categorias')
                    ->select('categorias.descripcion AS id','cate.descripcion AS parent','categorias.descripcion AS text')
                    ->where('categorias.idempresa',$user)
                    ->leftJoin('categorias as cate','categorias.idpadre','=','cate.idcategoria')
                    ->get();
        return response()->json($categorys);
    }

    public function create()
    {
        $user = Auth::user()->idempresa;
        $categorias = DB::table('categorias')
                    ->select('categorias.descripcion','categorias.color','cate.descripcion AS padre','cate.idcategoria AS idpadre','categorias.idcategoria','categorias.state')
                    ->where('categorias.idempresa',$user)
                    ->leftJoin('categorias as cate','categorias.idpadre','=','cate.idcategoria')
                    ->paginate(20);
        return view('categorys/categorys')->with('categorias',$categorias) ;
    }

    public function get_ID(Request $request){
        $descr = $request['descripcion'];
        $user = Auth::user()->idempresa;
        $categorias =DB::table('Categoria')->where('descripcion','like','%'.$descr.'%')->where('idempresa',$user)->first();
        return response()->json($categorias);
    }

    public function store(Request $request){
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $user = Auth::user()->idempresa;
        $idcategoria = $request['idcategoria'];
        $descr = $request['descripcion'];
        $idpadre = $request['idpadre'];
        $color = $request['color'];
        $editar = $request['edit'];

        if($editar){
            $category = Categoria::find($idcategoria);
            $category->descripcion = $descr;
            $category->idpadre = $idpadre;
            $category->idempresa = $user;
            $category->color = $color;

            $category->save();
        }else{
            $category = new Categoria;
            $category->descripcion = $descr;
            $category->idpadre = $idpadre;
            $category->idempresa = $user;
            $category->color = $color;

            $category->save();
        }



        return response()->json(['created'], 201);

    }
    public function show(Request $request){
    	$id = $request['id'];
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $user = Auth::user()->idempresa;

        $category = Categoria::where('idempresa',$user)->where('idcategoria',$id)->first();
      
        return  response()->json($category);
    }

    public function edit($id){
    	
    }
    public function update(Request $request){
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $user = Auth::user()->idempresa;

        $id = $request['id'];
        $descr = $request['descripcion'];
        $idpadre = $request['parent'];

        $category = Categoria::find($id);
        $category->descripcion = $descr;
         if($idpadre != null){
                if(!(bool)$this->is_child($id,$idpadre)){
                     $category->idpadre = $idpadre;
                }else{
                   return response()->json(['conflict'], 409); 
               }              
        }
        $category->idempresa = $user;

        $category->save();

        return response()->json(['accepted'], 202);
    }
    private function is_child($idNodo, $idParent){
        if($idParent == null || $idParent == 0){
            return false;
        }
        $data = DB::table('Categoria')
                ->where('idcategoria',$idParent)
                ->where('idpadre',$idNodo)->first();
        return is_object($data);
    }
    public function destroy(Request $request){
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $id = $request['id'];
        $category = Categoria::find($id);
        
        try {
        	$category->delete();
        	return response()->json(['accepted'], 202);
        } catch (Exception $e) {
        	return response()->json(['conflict'], 409);
        }

        
    }

   
}