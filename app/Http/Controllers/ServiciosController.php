<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use JWTAuth;

use Dingo\Api\Routing\Helpers;
use App\Models\Servicios;
use DB;
use App\User;
use Auth;


class ServiciosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = Auth::user()->idsucursal;
        $empresa = Auth::user()->idempresa;

        $cant = $request['cant'];
        $query = $request['query'];


        if(!isset($cant))$cant = 20;
        if(!isset($query)){
            $products = DB::table('producto')
                ->where('producto.tipo',2)
                ->where('idsucursal',$user)
                ->limit(1000)
                ->paginate(20);

        }else{
            $products = DB::table('producto')
                ->where('producto.tipo',2)
                ->where('producto.nombre','like','%'.$query.'%')
                ->where('idsucursal',$user)
                ->paginate($cant);
        }

        return view('servicios/lista_servicios', ['products' => $products]);

    }
    public function create(Request $request)
    {
         return view('servicios/nuevo_servicio');

    }

    public function update(Request $request){
        $query = $request['id'];
        $user = Auth::user()->idsucursal;
        $empresa = Auth::user()->idempresa;
        if(!empty($query)){

            $producto = DB::table('producto')
                ->where('idsucursal',$user)
                ->where('idempresa',$empresa)
                ->where('idproducto',$query)
                ->where('tipo',2)
                ->first();

            if( !empty($producto) or !isset($producto)){
                return view('servicios/editar_servicio',['mensaje' => '200'])->with('producto', $producto);
            }else{
                return json_encode(['mensaje' => '404']);
            }
        }
        else{
            return json_encode(['mensaje' => '404']);
        }
    }
    public function servicioInfo(Request $request){
        $query = $request['id'];
        $user = Auth::user()->idsucursal;
        $empresa = Auth::user()->idempresa;
        if(!empty($query)){

            $producto = DB::table('producto')
                ->where('idsucursal',$user)
                ->where('idempresa',$empresa)
                ->where('idproducto',$query)
                ->where('tipo',2)
                ->first();

            if( !empty($producto) or !isset($producto)){
                return view('servicios/info_servicio',['mensaje' => '200'])->with('producto', $producto);
            }else{
                return json_encode(['mensaje' => '404']);
            }
        }
        else{
            return json_encode(['mensaje' => '404']);
        }
    }
}