<?php
Route::post('ventas_query','VentaController@searchQuery');
Route::post('ventas_barcode','VentaController@searchBarcode');
Route::post('producto_detail','VentaController@productoVentaDetail');

Route::group(['middleware' => 'cors'], function(){
    Route::post('Auth_token','ApiAuthController@UserAuth');
    Route::get('prueba','PruebaController@index');



});

Route::group(['middleware' => ['web']], function () {

    Route::get('home', 'HomeController@index');
    Route::get('/', 'HomeController@index');
   
    Route::get('login/', function () {
        return view('login');
    });

    Route::get('mail',function(){
    	dd(Config::get('mail'));    	
    });
    Route::get('user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');




});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    //////////////////////////////Con LOGGIN///////////////////////////////
     //categorys resfull
    Route::get('categorys','CategoryController@index');
    Route::post('category_store','CategoryController@store');
    Route::get('category_get','CategoryController@show');
    Route::post('category_update','CategoryController@update');
    Route::post('category_delete','CategoryController@destroy');
    Route::get('categorysID','CategoryController@get_ID');

    Route::get('list_categorys','CategoryController@create');

    //product resfull
    Route::get('list_product/','ProductController@index');
    Route::get('producto_nuevo','ProductController@create');
    Route::get('producto_editar','ProductController@update');
    Route::post('product_store','ProductController@store');
    Route::post('product_delete','ProductController@destroy');
    Route::get('product_state','ProductController@status');
    Route::get('product_find','ProductController@find');
    Route::get('product_get','ProductController@get');
    Route::get('producto_info','ProductController@productoInfo');

    //Marcas
    Route::get('list_marcas/','MarcasController@create');
    Route::post('marca_store','MarcasController@store');
    Route::post('marca_delete','MarcasController@delete');
    Route::get('nueva_marca','MarcasController@nuevaMarca');
    Route::get('editar_marca','MarcasController@editarMarca');
    //MOTOS
    Route::get('list_motos/','MotosController@create');
    Route::get('nueva_moto','MotosController@nuevaMoto');
    Route::get('editar_moto','MotosController@editarMoto');
    Route::post('moto_store','MotosController@store');
    Route::post('moto_delete','MotosController@delete');
    //SERVICIOS
    Route::get('list_servicios/','ServiciosController@index');
    Route::get('servicio_nuevo/','ServiciosController@create');
    Route::get('servicio_editar','ServiciosController@update');
    Route::get('servicio_info','ServiciosController@servicioInfo');

    //CLIENTES
    Route::get('list_clientes','ClienteController@index');
    Route::get('nuevo_cliente','ClienteController@create');
    Route::get('editar_cliente','ClienteController@update');
    Route::get('eliminar_cliente','ClienteController@delete');
    Route::post('guardar_cliente','ClienteController@store');



    //transact resfull
    Route::get('transacts','TransactController@index');
    Route::post('transact_store','TransactController@store');
    Route::get('transact_get','TransactController@show');
    Route::put('transact_update','TransactController@update');
    Route::delete('transact_delete','TransactController@destroy');

    Route::get('ins','TransactController@create'); 
    Route::get('outs','TransactController@create');

    //caja
    Route::get('caja','CajaController@index');
    Route::get('buscarProducto','CajaController@buscar_producto');
    Route::get('buscarCliente','CajaController@buscar_cliente');
    Route::post('crear_caja','CajaController@store');
    Route::get('info_caja','CajaController@show');

    //inventario
    Route::get('entradas','InventarioController@entradas_index');
    Route::get('salidas','InventarioController@salidas_index');
    Route::get('buscarInventario','InventarioController@buscar_inventario');
    Route::post('crear_entrada','InventarioController@store_entradas');
    Route::post('crear_salida','InventarioController@store_salidas');


    //documentos
    Route::get('documentos','DocumentosController@showListado');
    Route::get('crear_documento','DocumentosController@showCreateDocumento');
    Route::post('get_descripcion','DocumentosController@getProductosServicios');
    Route::post('get_cliente_dni','DocumentosController@getClienteId');
    Route::post('get_cliente_ruc','DocumentosController@getClienteRuc');
    Route::post('print_proforma','DocumentosController@printProforma');
    Route::post('print_orden','DocumentosController@printOrden');



});

