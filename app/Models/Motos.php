<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Motos extends Model {
    protected $table = 'motos';
    protected $primaryKey = 'idmoto';
    public $timestamps = false;

}